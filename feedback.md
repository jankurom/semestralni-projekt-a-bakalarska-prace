PicoCluster user feedback
============================

Documentation
-------------
The draft of bachelor thesis I was working with to acquire general knowledge about both the hardware, and the software,
was well-writen, easy to read and orient in. It was more than sufficient to get the project working on local machine,
learn how the comunication works, and what the LED signals mean on BlinkIt module. There were some little pieces of
information I had to gain by myself by reading trough the files, but it was never anything major, or hard to find. I
will get to these specific cases later in the feedback.

Set up & Deployment
------
Set up was really easy, thanks to well documented and prepared ansible pipeline. I had a minor problem with accessing
the raspberry user, but the reason for that turned out to be only my bad reading, as I did not notice the part in the
thesis where it is written how to specify the user. There was also a badly indented code chunk in `full-deploy.yml`
file, which I had to correct. (Lines 44-46). Only thing I did not find in the documentation was the need to set the
server IP address in the Dockerfile, but that is very straightforward and hard to miss, so that was no problem.

The deployment itself took really long time (approximately 45 minutes, and it failed couple times), but I suppose that
is due to my usage of lower RaspberryPi model. The author of this project uses RaspberryPi 4B+, while I used RaspberryPi
3B, so I suppose the deployment time could be increased by this. The part of the deployment process which took the
longest was building the Docker image, but the copying of files to the node also took quite some time. I used only one
node.

It also occures to me, that the deployment procces might copy some unnecessary files to the node, as it copies the
server files, along with library and node projects.

It took me around 3 hours to deploy and run the project succesfully for the first time, but it was mainly because of the
lengthy deployment, which I had to restart couple times.

Running the project
-------------------
I had some minor issues with deploying the system succesfully, as I mentioned above, which resulted in wrongly set IPs,
but I was able to fix that quite easily. The node started up really well, but I had a slight problem with starting the
Server, as the path to the configuration file is hard coded in the code, and I was launching the application from
upper-level directory in my IntelliJ project. It was however quite easy to spot and fix this. Afther that, I was quicky
able to connect the node, and start assigning tasks.


Algorithms
----------
On the first try, I was not able to succesfully finish any algorithm, because I was using only one node, and the server
asked for five at the least, which took me by surprise. I navigated trought the code, and discovered that the number of
Nodes required was set in `TaskWindow.java` on line 97, and was dependent on a default value of a GUI JSpinner, which
was responsible for listing the nodes in gui (if I understand it correctly). Overally, this was the only bigger flaw in
the whole system, and it might even be caused by me not setting something correctly, but it was a little bit surprising
nontheless.

After fixing the node limit, I was able to succesfully run all the prepared algirithms with no problem. Sometimes,
especially the UDPTester and BlinkIt pattern did not seem to finish, and docker logs on the node showed an UDP timeout,
which might be due to the usage of only one node. However, I'm not really sure.

I tried to create my own pattern for the BlinkIt task, which was no problem at all, and was done in less than 5 minutes.
After that, I tried to create my own little task, which would light the BlinkIt module as a progress bar of
randomly-long interval. That was no problem, thanks to well written and readable code, and I was done in less than 20
minutes.

Code
-----
Overally, the code is very well written and documented, which makes it really easy to read and orient even on first read
trough. Along with great java doc, comments and correct java principles, it was no problem to get to know the
implementation details and implement my own parts.

Next plans
----------
I am going to use the PicoCluster project im my own project, which will result in my bachelor thesis. Im going to
rewrite a distributed multi-agent planning algorithm [COBRA](https://ojs.aaai.org/index.php/ICAPS/article/view/13696),
which is currently implemented only as a multi-thread aplication, to work as a trully distributed system on a
RaspberryPi cluster. Furthermore, I am going to test and discuss various conditions and variables, which might result in
significant differences between the result of simulated distributed system, and truly distributed system on multiple
standalone devices.    