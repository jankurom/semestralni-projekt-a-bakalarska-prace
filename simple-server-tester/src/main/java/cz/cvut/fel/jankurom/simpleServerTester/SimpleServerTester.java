/*
 * Written by Roman Janků at Faculty of Electrical Engineering at Czech Technical University at Prague for purpose of bachelor thesis.
 */

package cz.cvut.fel.jankurom.simpleServerTester;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * Program used for testing simple servers.
 *
 * @author Roman Janků
 */
public class SimpleServerTester {

  /**
   * Logger for logging events.
   */
  private static final Logger LOGGER = Logger.getLogger("cz.cvut.fel.jankurom.simpleServerTester");
  /**
   * List of addresses to test.
   */
  private static final List<String> addresses = new ArrayList<>();
  /**
   * Port which to test.
   */
  private static int port = 54321;

  /**
   * Main method of this program.
   *
   * @param args Arguments from command line.
   */
  public static void main(String... args) {
    //  Read arguments if there are any.
    if (args.length > 0) {
      parseOptions(args);
    }
    //  If there are any addresses cycle through them and test them. Terminate otherwise.
    if (addresses.size() > 0) {
      addresses.forEach(SimpleServerTester::testServer);
    } else {
      LOGGER.info(() -> "No server to test. Program is terminating.");
    }
  }

  /**
   * Method for parsing arguments from command line.
   *
   * @param args Arguments from command line.
   */
  private static void parseOptions(String... args) {
    try {
      //  Cycle through arguments and parse them.
      for (int i = 0; i < args.length; i++) {
        switch (args[i]) {
          //  Set different port to test,
          case "-p":
            i++;
            try {
              port = Integer.parseInt(args[i]);
            } catch (NumberFormatException ex) {
              String arg = args[i];
              LOGGER.warning(() -> "Cannot parse port number because '" + arg + "' is not a valid port number.");
            }
            break;
          //  Set address to test.
          case "-a":
            i++;
            addresses.add(args[i]);
            break;
          //  Parse file with addresses.
          case "-f":
            i++;
            parseFile(new File(args[i]));
            break;
          //  Unknown argument.
          default:
            String arg = args[i];
            LOGGER.warning(() -> "Unknown option '" + arg + "'. This option is ignored.");
        }
      }
    } catch (ArrayIndexOutOfBoundsException ex) {  //  Wrong number of argument supplied.
      LOGGER.severe(() -> "Wrong number of parameters. One more is expected.");
    }
  }

  /**
   * Method for testing server.
   *
   * @param address Address on which the server is running.
   */
  private static void testServer(String address) {
    Socket socket;
    BufferedReader reader;
    //  Try connecting and reading whatever the server sends.
    try {
      LOGGER.info(() -> "Connecting to '" + address + "' on port '" + port + "'.");
      socket = new Socket(address, port);
      reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
      String line = reader.readLine();
      LOGGER.info(() -> "Received: " + line);
    } catch (UnknownHostException ex) {  //  Host could not be found.
      LOGGER.warning(() -> "Host could not be resolved.");
    } catch (IOException ex) {  //  Some other exception occurred when communicating with server.
      LOGGER.warning(() -> "Exception '" + ex.getMessage() + "' occurred.");
    }
  }

  /**
   * Parse the file with server's addresses.
   *
   * @param file File from which to parse.
   */
  private static void parseFile(File file) {
    //  Read file line by line.
    try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
      String line;
      while ((line = reader.readLine()) != null) {
        //  Test if the line is not comment (starting with #) and has sufficient length.
        if (line.length() > 6 && !line.startsWith("#")) {
          addresses.add(line.trim());
        }
      }
    } catch (FileNotFoundException ex) {  //  File does not exist.
      LOGGER.warning(() -> "File '" + file.getName() + "' could not be found.");
    } catch (IOException ex) {  // Other exception when reading the file.
      LOGGER.warning(() -> "Exception '" + ex.getMessage() + "' occurred when reading file '" + file.getName() + "'.");
    }
  }
}
