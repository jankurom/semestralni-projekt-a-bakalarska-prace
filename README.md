# Semestral project a Bachelor thesis

**Name:** Distributed computations on RaspberryPi cluster  
**Author:** Roman Janků (jankurom@fel.cvut.cz)  
**Supervisor:** doc. Ing. Jiří Vokřínek, Ph.D. (jiri.vokrinek@fel.cvut.cz)  
**Department of supervisor:** Department of Computer Science

This is a repository containing my semester project and bachelor thesis which I continued to work on even after getting
my bachelor degree. If you are interested in my bachelor thesis you can find the `bachelor.tex` file in the `documents`
directory. Source code used in bachelor thesis is in *Bachelor thesis* release in the repository. If you are interested
in the latest version of this project use sources available in the latest commit on the `main` branch in this
repository. Up-to-date documentation can be found in `documentation.tex` file in `documents` directory in the
repository.

## Files and directories

Key directories and files are listed below:

* **documents** contains all documentation files and thesis files. `thesis.tex` is my semester work, `assignment.pdf` is
  assignment of my bachelor thesis, `bachelor.tex` is my bachelor thesis and `documentation.tex` is up-to-date
  documentation of this project.
* **picocluster-distributed-algorithm** contains the source code of the platform for running distributed algorithms.
  Ansible, Docker and Maven scripts are included. It is a maven project with three modules `server` contains server
  application, `node` is the node application deployed to RaspberryPi cluster and `library` contains common code for
  both server and node.
* **presentation** contains presentations used for work defenses of semester project and bachelor thesis.
* **simple-server** and **simple-server-tester** are testing applications that were used early in development to test
  the cluster functionality. Both are maven projects.
* **README.md** is this readme file.
* **install.sh** is an installation script that will clone this repository and install all dependencies.

## Cloning this repository

You can close this repository manually using `git clone` command, or you can use `install.sh` script. This script will
not only clone this repository, but it will also install all dependencies. The repository is cloned in the same
directory as the script is run in. After cloning you have all necessary source codes on your computer. You can either
build them or modify them and build them.

**BE CAREFULL!** `install.sh` is written for Debian and uses `apt` to install dependencies. If you are using different
Linux distribution or different operating system you probably need to modify this script.

## Building the project

Building the project is done automatically using Maven. To build the project `maven` and `openjdk-11-jdk` or compatible
Java development kit is needed. If you used `install.sh` script you have everything already installed. Otherwise, you
would need to install these dependencies manually.

You can build the project by running `maven package` command in the `picocluster-distributed-algorithm` directory. This
will download all libraries and create runnable `.jar` files. Server file is
in `picocluster-distributed-algorithm/server/target/server-0.9-jar-with-dependencies.jar` and node file is
in `picocluster-distributed-algorithm/node/target/node-0.9-jar-with-dependencies.jar`.

**KNOWN ISSUE:** For some reason when project is built for the first time it complains about missing library. When
compiled for the second time everything finishes OK. Cause of this problem is not known.

## Deploying the project

Deployment is done automatically using Ansible. If you used `install.sh` script you have it installed. Otherwise, you
would need to install `ansible` and `sshpass` to use ssh with password successfully. To deploy the project you first
need to build it to create node `.jar` file that will be then distributed.

If you deploy to the RaspberryPis for the first time, use `deploy-full.yml` script. This will install all needed
dependencies and will deploy the server. For subsequent deployment after changing the source code use `deploy.yml`
script as it omits the installation of dependencies that would make the deployment unnecessary long.

To deploy use
command `ansible-playbook -i ./hosts -kK --ssh-extra-args='-o "PubkeyAuthentication=no"' -u picocluster SCRIPT`
where `SCRIPT` is the `.yml` script you want to run. You might need to change IP addresses in the `hosts`. By
default `[cluster]` group of addresses is used.

Make sure you run the server application first. If the node application does not find server application it fails. For
more information about deployment see `documentation.tex` in `documents`.

## Using the project

You can use this project to run distributed algorithms on RaspberryPis in PicoCluster or after some modification on any
other cluster. To see how the applications work and communicate and how to implement custom algorithms
see `documentation.tex` in `documents`. Semester project and bachelor thesis do not contain latest changes.