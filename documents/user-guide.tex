\documentclass[11pt]{article}

\usepackage{graphicx}
\graphicspath{ {./img/} }

\usepackage{listings}
\usepackage{wrapfig}
\usepackage{datetime}
\usepackage{xcolor}
\usepackage{hyperref}
\usepackage{float}

\usepackage{url}
\makeatletter
\g@addto@macro{\UrlBreaks}{\UrlOrds}
\makeatother

\usepackage{etoolbox}
\makeatletter
\preto{\@verbatim}{\topsep=11pt \partopsep=0pt }
\makeatother

\clubpenalty = 10000
\widowpenalty = 10000
\displaywidowpenalty = 10000

\lstset{
    basicstyle=\ttfamily,
    columns=fullflexible,
    breaklines=true,
    postbreak=\mbox{\textcolor{orange}{$\hookrightarrow$}\space}
}

\title{Distributed computations on RaspberryPi platform}
\author{Roman Janků}

\begin{document}

    \maketitle

    \tableofcontents

    \begin{abstract}
    This document describes the application for distributed computing on RaspberryPi platform from a user point of view. It describes how to launch the application, how to run tasks and how to gather results.
    \end{abstract}

    \begin{figure}[H]
        \includegraphics[width=\textwidth]{cluster}
        \caption{Pico 5 cluster}
        \label{fig:cluster}
    \end{figure}

    \section{Starting application}

    The list below describes how to launch application for distributed computations on RaspberryPi cluster. All commands are run from \\ \texttt{picocluster-distributed-algorithm} directory in the root of the repository.
    
    \begin{enumerate}
    	\item Start the server application using 
    	
		\begin{lstlisting}
java -jar server/target/server-0.9-jar-with-dependencies.jar
    	\end{lstlisting}
    	
    	command.
    	
    	\item Start the client application on RaspberryPis using Ansible. This can be done using 
    	
		\begin{lstlisting}
ansible-playbook -i ./hosts -kK --ssh-extra-args='-o "PubkeyAuthentication=no"' -u picocluster <SCRIPT_NAME>
    	\end{lstlisting}  
    	
    	command, where \texttt{<SCRIPT\_NAME>} is name of a script you want to run.	
    	
    	\begin{enumerate}
    		\item If you want to run the application that is currently on cluster, use \texttt{restart.yml} script.
    		\item If you want to deploy and run a new version, that have been compiled, use \texttt{deploy.yml} script.
    		\item If you want to deploy to a new RaspberyPis that do not have dependencies installed, use \texttt{deploy-full.yml} script.
    	\end{enumerate}
    	\item After Ansible finishes and all clients are shown in the list in the main server window, the application is ready to run distributed algorithms. See following sections on how to run the algorithms.
    	\item If you want to stop the client applications, use \texttt{stop.yml} script.
    \end{enumerate}
    
    To quickly see in what state the nodes are, Blinkt! modules are connected to the RaspberryPis. Meaning of the LEDs and their colors is described in a subsection below.
    
    \subsection{Blinkt! module}

    \begin{figure}[H]
        \includegraphics[width=\textwidth]{blinkt}
        \caption{Picture of~Blinkt! module}
        \label{fig:blinkt}
    \end{figure}

    If not overridden by~the~algorithm running on~the~node the~Blinkt! module shows current state of~node application. The~meaning of~each LED is described below. The~LEDs are numbered from~left to~right beginning with~zero and~ending with~seven if placed the same way as can be seen on~\autoref{fig:blinkt}.

    \begin{itemize}
        \item \textit{LED 0} lights up green if the~node application has started successfully.
        \item \textit{LED 1} lights up green when connection to~the~server was established or~yellow if the~connection was interrupted.
        \item \textit{LED 2} lights up green if node has assigned ID and~is ready to participate in~distributed algorithms.
        \item \textit{LED 3} and \textit{LED 4} light up red when the~node application has terminated.
        \item \textit{LED 5} lights up red if the~computation failed on~the~node.
        \item \textit{LED 6} lights up red if the~computation was stopped by~server.
        \item \textit{LED 7} lights up yellow, when the~node is doing computations in~distributed algorithms and green if it has finished the~distributed algorithm.
    \end{itemize}


    \section{Graphical user interface of the server}

    \begin{figure}[H]
        \includegraphics[width=\textwidth]{gui}
        \caption{Graphical user interface of server application}
        \label{fig:gui}
    \end{figure}

    Graphical user interface of~server is show on~\autoref{fig:gui} and~bellow is description of~its~components.

    \begin{enumerate}
        \item List of~connected nodes
        \item Buttons to~control nodes (\textit{Kick} kicks selected node from~server, \textit{Suspend} changes selected node to~suspended state and~\textit{Wake} changes selected node to~ready state)
        \item Table with~information about~selected node
        \item List of~all tasks on~the~server
        \item Buttons to~control tasks (\textit{Add} opens a~dialog for~adding a~new task, \textit{Stop} stops selected running task or~removes ready task, ~\textit{Export} exports all tasks to~JSON file and \textit{Load tasks} loads tasks from JSON file)
        \item Table with~details about~selected task
        \item List of~logs
    \end{enumerate}


    \section{Running tasks}

    Click the~\textit{Add} button in~the~main server window. A dialog for adding new task will appear as~shown on~the~\autoref{fig:addtask}.

    \begin{figure}[H]
        \includegraphics[width=\textwidth]{dialog}
        \caption{Dialog for adding new task}
        \label{fig:addtask}
    \end{figure}

    Fill the dialog as~follows

    \begin{enumerate}
        \item Use the~combobox to~selected algorithm you want to~run.
        \item Fill the~table with~parameters for~the~algorithm. All inputs need to~be confirmed by~pressing \texttt{Enter} key. Otherwise, the~input will not be saved.
        \item Select the~number of~nodes you want the~algorithm to~run~on.
    \end{enumerate}

    After~filling the~dialog click \textit{Run} to~add the~task to~queue. The~added task will be started as~soon as~it will be first in~the~queue and~sufficient number of~nodes in~ready state will be available. The~task will fail if~the~number of~connected nodes is smaller than~number of~nodes on~which the~task should run. To~close the~dialog without~adding new task click \textit{Cancel} button.


    \section{Importing tasks}

    Tasks can be imported from JSON file using \textit{Load tasks} button. The imported tasks are placed in the same queue as tasks entered using dialog. Tasks are started when there enough nodes available. The format of the file with imported tasks should hav this format

    \begin{lstlisting}
[
	{
		"name": "<NAME_OF_THE_TASK>",
		"fullName": "<TASK_FULL_NAME>",
		"parameters": [
			"<PARAMETER>"		
		],
		"nodeCount": "<NUMBER_OF_NODES_TASK_SHOULD_RUN_ON>"	
	}
]
    \end{lstlisting}


    \section{Exporting results}

    Data gathered from~distributed algorithms can be exported from~the~program using the~\textit{Export} button. Program asks for~location and~name of~the~file where the~data will be exported. After~dialog confirmation all results are written in~JSON format to~the~file.

    All tasks are exported using this method including \texttt{READY} and \texttt{RUNNING} tasks.

    The~file has following format

    \begin{lstlisting}
[
    {
        "partialResults": [
            "result1",
            "result2"
        ].
        "name": "<NAME_OF_TASK>",
        "nodes": <NODE_COUNT>,
        "result": "<TASK_RESULT>",
        "state": "<TASK_STATE>",
        "fullName": "<FULL_CLASS_NAME>",
        "startTime": "<START_TIME>",
        "endTime": "<END_TIME>",
        "parameters": [
            "param1",
            "param2"
        ]
    }
]
    \end{lstlisting}

\end{document}