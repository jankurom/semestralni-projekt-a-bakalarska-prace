/*
 * Written by Roman Janků at Faculty of Electrical Engineering at Czech Technical University at Prague for purpose of bachelor thesis.
 */

package cz.cvut.fel.jankurom.simpleServer;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.util.UUID;
import java.util.logging.Logger;

/**
 * Simple server used for testing Ansible and Docker.
 *
 * @author Roman Janků
 */
public class SimpleServer {
  /**
   * Port on which server listens.
   */
  private static final int PORT = 54321;
  /**
   * Logger for logging events into console.
   */
  private static final Logger LOGGER = Logger.getLogger("cz.cvut.fel.jankurom.simpleServer");
  /**
   * Unique identification of this server.
   */
  private static final String id = UUID.randomUUID().toString();

  /**
   * Main method of the program. Everything happens here.
   *
   * @param args Arguments from the command line. They are ignored.
   */
  public static void main(String... args) {
    LOGGER.info(() -> "App is starting ...");

    //  Open server socket on specified port.
    ServerSocket socket;
    try {
      socket = new ServerSocket(PORT);
    } catch (IOException ex) {
      LOGGER.severe(() -> "Server socket could not be opened (" + ex.getMessage() + ")!");
      return;
    }
    LOGGER.info(() -> "App successfully started.");

    //  Listen on the socket for clients that are connecting.
    while (true) {
      try {
        //  Send my identification to the client.
        var client = socket.accept();
        var out = new PrintWriter(client.getOutputStream(), true);
        out.println("Server: " + id + "\n");
        LOGGER.info(() -> "Client " + socket.getInetAddress() + " connected, sending identification and closing connection.");
        //  Close connection to the client.
        out.close();
        client.close();
      } catch (IOException ex) {
        LOGGER.severe(() -> "Error when opening connection to client (" + ex.getMessage() + ")!");
      }
    }
  }
}
