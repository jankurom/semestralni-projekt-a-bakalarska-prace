#!/bin/sh

echo =========================
echo   Instal building tools
echo =========================

sudo apt install git maven openjdk-11-jdk

echo ====================
echo   Clone repository
echo ====================

git clone https://gitlab.fel.cvut.cz/jankurom/semestralni-projekt-a-bakalarska-prace.git

echo =======================
echo   Instal deploy tools
echo =======================

sudo apt install ssh sshpass ansible