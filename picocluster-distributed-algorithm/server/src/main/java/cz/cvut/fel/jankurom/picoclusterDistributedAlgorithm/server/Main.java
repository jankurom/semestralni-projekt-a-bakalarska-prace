package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server;

import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.exceptions.ConfigurationException;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.gui.MainWindow;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.server.Server;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 * Main class of the server with main method.
 *
 * @author Roman Janků (jankurom@fel.cvut.cz)
 */
public class Main {

  /**
   * Logger used for logging events happening in the app.
   */
  private static final Logger LOGGER = Logger.getLogger("picocluster-server");

  /**
   * Main method of the server.
   *
   * @param args Arguments from command line.
   */
  public static void main(String... args) {

    LOGGER.info(() -> "Application is starting ...");

    Runtime.getRuntime().addShutdownHook(new Thread(() -> Server.getInstance().terminate()));

    try {
      LOGGER.info(() -> "Loading configuration ...");
      Configuration.load();
      LOGGER.info(() -> "Configuration successfully loaded.");
    } catch (ConfigurationException ex) {
      LOGGER.severe(() -> "Error while loading configuration: " + ex.getMessage() + ". Application will terminate.");
      JOptionPane.showMessageDialog(null,
          "Configuration error occurred and the program cannot start. \n The message is: " + ex.getMessage(),
          "Configuration error!",
          JOptionPane.ERROR_MESSAGE);
      return;
    }

    LOGGER.info(() -> "Opening main window ...");
    new MainWindow();
    LOGGER.info(() -> "Application successfully started.");

    LOGGER.info(() -> "Server is starting ...");
    Server.getInstance().start();
    LOGGER.info(() -> "Server started.");
  }
}
