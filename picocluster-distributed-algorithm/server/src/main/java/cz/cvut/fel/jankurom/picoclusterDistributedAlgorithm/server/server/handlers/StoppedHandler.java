package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.server.handlers;

import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.Message;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.MessageType;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.server.Client;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.server.Server;

/**
 * Handler for STOPPED messages.
 *
 * @author Roman Janků (jankurom@fel.cvut.cz)
 */
public class StoppedHandler extends AbstractHandler {

  public StoppedHandler() {
    handledMessageType = MessageType.STOPPED;
  }

  @Override
  public void handleMessage(Message message, Client client) {
    Server.getInstance().stoppedTask(client);
  }
}