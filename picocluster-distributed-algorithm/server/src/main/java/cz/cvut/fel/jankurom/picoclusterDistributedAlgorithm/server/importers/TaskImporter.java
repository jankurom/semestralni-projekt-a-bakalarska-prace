package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.importers;

import com.google.gson.Gson;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public final class TaskImporter {

  private static final Gson gson = new Gson();

  private TaskImporter() {
  }

  public static List<ImportedTask> importFromFile(String file) throws FileNotFoundException {
    return new ArrayList<>(java.util.Arrays.asList(gson.fromJson(new FileReader(file), ImportedTask[].class)));
  }
}
