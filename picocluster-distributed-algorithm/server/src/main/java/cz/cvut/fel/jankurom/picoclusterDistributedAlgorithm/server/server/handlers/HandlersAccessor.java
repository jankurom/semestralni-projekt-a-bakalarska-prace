package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.server.handlers;

import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.MessageType;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.exceptions.NoHandlerForMessageTypeException;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.logging.Logger;
import org.reflections.Reflections;

/**
 * Class for accessing handlers for messages.
 *
 * @author Roman Janků (jankurom@fel.cvut.cz)
 */
public class HandlersAccessor {

  private static final Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

  /**
   * Reflections for accessing and loading the handlers.
   */
  private static final Reflections REFLECTIONS;
  /**
   * List of loaded handlers.
   */
  private static final Map<MessageType, AbstractHandler> HANDLERS;

  static {
    REFLECTIONS = new Reflections("cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.server.handlers");
    HANDLERS = new HashMap<>();
    REFLECTIONS.getSubTypesOf(AbstractHandler.class).stream()
        .map(h -> {
          try {
            return h.getDeclaredConstructor().newInstance();
          } catch (InstantiationException | IllegalAccessException | InvocationTargetException |
                   NoSuchMethodException ex) {
            LOGGER.warning(() -> "Could not load '" + h.getName() + "'. Cause: " + ex.getMessage());
          }
          return null;
        })
        .filter(Objects::nonNull)
        .forEach(h -> {
          if (!HANDLERS.containsKey(h.getHandledMessageType())) {
            HANDLERS.put(h.getHandledMessageType(), h);
          }
        });
  }

  /**
   * Returns handler for given MessageType or null if there is no handler for given MessageType.
   *
   * @param messageType MessageType to look for.
   * @return handler for MessageType or null.
   */
  public static AbstractHandler getHandlerForMessageType(MessageType messageType) throws NoHandlerForMessageTypeException {
    if (hasHandlerForMessageType(messageType)) {
      return HANDLERS.get(messageType);
    }
    throw new NoHandlerForMessageTypeException("No handler for '" + messageType + "' type of message.");
  }

  /**
   * Tells weather there is handler for given MessageType.
   *
   * @param messageType MessageType to look for.
   * @return TRUE if there is handler for given MessageType, FALSE otherwise.
   */
  public static boolean hasHandlerForMessageType(MessageType messageType) {
    return HANDLERS.containsKey(messageType);
  }
}