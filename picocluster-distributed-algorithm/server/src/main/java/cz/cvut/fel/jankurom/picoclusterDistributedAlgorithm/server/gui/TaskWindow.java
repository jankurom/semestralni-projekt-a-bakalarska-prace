package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.gui;

import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.AbstractAlgorithm;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.Algorithms;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.server.Server;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.SpinnerNumberModel;
import javax.swing.table.DefaultTableModel;

/**
 * Dialog for running tasks.
 *
 * @author Roman Janků (jankurom@fel.cvut.cz)
 */
class TaskWindow extends JDialog {

  private JComboBox<AbstractAlgorithm> tasks;
  private JButton run;
  private JButton cancel;
  private JTable table;
  private JSpinner nodes;

  private DefaultTableModel model;

  /**
   * Opens a new task window.
   */
  TaskWindow() {
    setTitle("Start new task");
    setSize(new Dimension(600, 400));
    setMinimumSize(new Dimension(400, 300));
    setModal(true);

    buildGui();

    setVisible(true);
  }

  /**
   * Builds gui of the window.
   */
  private void buildGui() {
    setLayout(new BorderLayout(5, 5));

    tasks = new JComboBox<>();
    tasks.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
    Algorithms.getAllAlgorithms().forEach(a -> tasks.addItem(a));
    add(tasks, BorderLayout.NORTH);

    model = new DefaultTableModel(new String[][]{}, new String[]{"Parameter", "Value"});
    table = new JTable(model) {
      public boolean isCellEditable(int row, int column) {
        return column == 1;
      }

      public boolean getScrollableTracksViewportWidth() {
        return getPreferredSize().width < getParent().getWidth();
      }
    };
    table.setFillsViewportHeight(true);
    changedAlgorithm();
    JScrollPane scrollPane = new JScrollPane(table);
    scrollPane.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEmptyBorder(0, 5, 0, 5),
        scrollPane.getBorder()));
    add(scrollPane, BorderLayout.CENTER);

    tasks.addActionListener(e -> changedAlgorithm());

    JPanel buttonPanel = new JPanel(new FlowLayout());

    run = new JButton("Run");
    run.addActionListener(e -> clickedRun());
    buttonPanel.add(run);

    cancel = new JButton("Cancel");
    cancel.addActionListener(e -> dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING)));
    buttonPanel.add(cancel);

    JPanel bottomPanel = new JPanel();
    bottomPanel.setLayout(new BoxLayout(bottomPanel, BoxLayout.Y_AXIS));

    nodes = new JSpinner(new SpinnerNumberModel(5, 1, Integer.MAX_VALUE, 1));

    bottomPanel.add(nodes);
    bottomPanel.add(buttonPanel);
    add(bottomPanel, BorderLayout.SOUTH);
  }

  /**
   * User clicked on run button.
   */
  private void clickedRun() {
    List<String> parameters = new ArrayList<>();
    for (int row = 0; row < table.getRowCount(); row++) {
      parameters.add((String) table.getValueAt(row, 1));
    }
    AbstractAlgorithm algo = (AbstractAlgorithm) tasks.getSelectedItem();
    Server.getInstance().addTask(algo.name(), algo.getClass().getName(), parameters.toArray(String[]::new), (int) nodes.getModel().getValue());
    dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
  }

  /**
   * User selected another algorithm.
   */
  private void changedAlgorithm() {
    for (int i = model.getRowCount() - 1; i >= 0; i--) {
      model.removeRow(i);
    }

    var algorithm = (AbstractAlgorithm) tasks.getSelectedItem();
    Arrays.stream(algorithm.parameters()).forEach(p -> model.addRow(new String[]{p, ""}));
    table.updateUI();
  }
}