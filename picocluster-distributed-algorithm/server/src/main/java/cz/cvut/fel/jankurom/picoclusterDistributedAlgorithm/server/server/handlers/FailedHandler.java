package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.server.handlers;

import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.Message;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.MessageType;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.server.Client;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.server.Server;

/**
 * Handler for FAILED messages.
 *
 * @author Roman Janků (jankurom@fel.cvut.cz)
 */
public class FailedHandler extends AbstractHandler {

  public FailedHandler() {
    handledMessageType = MessageType.FAILED;
  }

  @Override
  public void handleMessage(Message message, Client client) {
    Server.getInstance().failedTask(client, message.getContent());
  }
}
