package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.exceptions;

/**
 * Thrown when node requests UUID that is already in use.
 *
 * @author Roman Janků (jankurom@fel.cvut.cz)
 */
public class UuidAlreadyInUse extends Exception {
  public UuidAlreadyInUse() {
  }

  public UuidAlreadyInUse(String message) {
    super(message);
  }
}
