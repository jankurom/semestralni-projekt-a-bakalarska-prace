package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.server;

import com.google.gson.Gson;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.AbstractAlgorithm;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.ServerPartListener;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.Message;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.MessageType;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.dto.NodeDto;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.dto.StartTask;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.Configuration;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.exceptions.AlreadyHasUuidException;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.exceptions.NoDataServerIsRunning;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.exceptions.RecepientDoesNotExist;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.exceptions.ReservedUuidException;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.exceptions.UuidAlreadyInUse;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.listeners.ClientListener;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.listeners.ServerListener;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.server.distributor.Distributor;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * This class is singleton.
 *
 * @author Roman Janků
 */

public final class Server extends Thread implements ClientListener, ServerPartListener {

  /**
   * Logger used for logging events happening in the app.
   */
  private static final Logger LOGGER = Logger.getLogger("picocluster-server");
  /**
   * The only instance of this class as this is singleton.
   */
  private static Server instance = null;
  /**
   * List of clients.
   */
  private final List<Client> clients = new ArrayList<>();
  /**
   * List of listeners.
   */
  private final List<ServerListener> listeners = new ArrayList<>();
  /**
   * This is a lock that everyone must obtain when he wants to work with clients.
   */
  private final Object clientLock = new Object();
  /**
   * Timer for the client update task.
   */
  private final Timer timer = new Timer();
  /**
   * Gson for parsing JSON into Java objects and serializing Java objects into JSON.
   */
  private final Gson gson;
  /**
   * List of tasks.
   */
  private final List<Task> tasks;
  /**
   * Tells weather the server is running.
   */
  private boolean running = false;
  /**
   * Tells weather clients changed.
   */
  private volatile boolean newClients = false;
  /**
   * Index of the current task. Points one index above the last task in list if all tasks have finished (or failed).
   */
  private volatile int nextTask = 0;

  /**
   * Hide constructor because this is singleton.
   */
  private Server() {
    gson = new Gson();
    tasks = new ArrayList<>();

    try {
      Distributor.getDistributor().startDistributorServer();
    } catch (IOException ex) {
      // TODO
    }
  }

  /**
   * Get the only instance of this class.
   *
   * @return The only instance of this class.
   */
  public static Server getInstance() {
    if (instance == null) {
      synchronized (Server.class) {
        if (instance == null) {
          instance = new Server();
        }
      }
    }
    return instance;
  }

  @Override
  public void run() {

    LOGGER.info(() -> "Starting server ...");

    ServerSocket serverSocket;

    try {
      //  Start the timer for client updates.
      timer.schedule(new TimerTask() {
        @Override
        public void run() {
          updateClients();
        }
      }, 5000, 2000);

      //  Create the server socket.
      serverSocket = new ServerSocket(Integer.parseInt(Configuration.getValue("port")));
      listeners.forEach(ServerListener::serverStarted);
      running = true;

      LOGGER.info(() -> "Server started, listening for clients.");

      while (running) {
        try {
          Socket socket = serverSocket.accept();
          synchronized (clientLock) {
            Client client = new Client(socket);
            client.start();
            clients.add(client);
            client.addClientListener(this);
            listeners.forEach(l -> l.clientsChanged("New client " + client));
            LOGGER.info(() -> "New client from " + socket.getInetAddress() + ":" + socket.getPort() + " with UUID" +
                client.getUuid());
          }
        } catch (SocketTimeoutException ex) {
          //  This exception is thrown so the server can be stopped
          LOGGER.fine(() -> "Server socket timed out.");
        } catch (IOException ex) {
          LOGGER.severe(() -> "Exception '" + ex.getMessage() + "' occurred on server socket. will terminate.");
          terminate();
        }
      }
    } catch (IOException ex) {
      LOGGER.severe(() -> "Server cannot be started: " + ex.getMessage());
      terminate();
    }
  }

  /**
   * Returns list of connected clients.
   *
   * @return List of connected clients.
   */
  public Client[] getClients() {
    return clients.toArray(Client[]::new);
  }

  public Task[] getTasks() {
    return tasks.toArray(Task[]::new);
  }

  /**
   * Kicks client from server.
   *
   * @param client Client to kick.
   */
  public void kickClient(Client client) {
    client.disconnect();
  }

  /**
   * Removes client from server's list. This calls the client it self when terminating. If you want to remove client from server use kickClient() method insted.
   *
   * @param client Client to remove.
   */
  public void removeClient(Client client) {
    synchronized (clientLock) {
      if (client.getClientState() == ClientState.COMPUTING) {
        failedTask(client, "Node disconnected!");
      }

      LOGGER.fine(() -> "Removing client " + client + ".");
      clients.remove(client);
      listeners.forEach(l -> l.clientsChanged("Client " + client + " disconnected."));
      sendClientsInfo();
    }
  }

  /**
   * Updates clients and informs them weather something changed.
   */
  private void updateClients() {
    //  When computing we don't want to add new nodes. Also, when clients didn't change there is no point updating.
    if (newClients) {
      synchronized (clientLock) {
        LOGGER.fine(() -> "Client update timer.");
        //  Change status of newly connected clients
        clients.stream()
            .filter(Client::isConnected)
            .filter(Client::hasUuid)
            .forEach(c -> {
              c.setClientState(ClientState.READY);
              LOGGER.fine(() -> "Changed clients state to READY on " + c);
            });
        //  Inform all clients about connected clients
        sendClientsInfo();

        listeners.forEach(l -> l.clientsChanged("Updated connected clients with UUID to ready state."));
        newClients = false;
        triggerTaskStart();
      }
    }
  }

  /**
   * This method stops server.
   */
  public void terminate() {
    running = false;
  }

  /**
   * Adds new server listener.
   *
   * @param listener Listener to add.
   */
  public void addListener(ServerListener listener) {
    listeners.add(listener);
  }

  /**
   * Assigns given UUID to a given client.
   *
   * @param uuid   UUID to assign.
   * @param client Client to whom assign UUID.
   * @throws ReservedUuidException When the UUID is reserved.
   * @throws UuidAlreadyInUse      When the UUID is already in use.
   */
  public void assignUuidToClient(String uuid, Client client) throws ReservedUuidException, UuidAlreadyInUse, AlreadyHasUuidException {
    if (client.getUuid() != null) {
      throw new AlreadyHasUuidException("Already has an UUID with value: " + client.getUuid().toString() + "!");
    } else if (uuid.equalsIgnoreCase("server")) {
      throw new ReservedUuidException("'server' is a reserved UUID for server and cannot be assigned to node!");
    } else if (uuid.isEmpty()) {
      throw new ReservedUuidException("UUID cannot be empty!");
    }
    synchronized (clientLock) {
      var count = clients.stream()
          .filter(Client::hasUuid)
          .filter(c -> c.getUuid().toString().equals(uuid))
          .count();

      if (count != 0) {
        throw new UuidAlreadyInUse();
      } else {
        client.setUuid(UUID.fromString(uuid));
        newClients = true;
        listeners.forEach(l -> l.clientsChanged("Assigned UUID to " + client));
      }
    }
  }

  /**
   * Add a new task to the queue and run it when sufficient number of nodes is available.
   *
   * @param name       Name of the task.
   * @param fullName   Full name of the class to be run.
   * @param parameters Parameters for the task.
   * @param nodeCount  Number of nodes on which it should run.
   */
  public void addTask(String name, String fullName, String[] parameters, int nodeCount) {
    var task = new Task();
    task.setName(name);
    task.setFullName(fullName);
    task.setNodes(nodeCount);
    task.setParameters(parameters);
    task.setState(TaskState.READY);
    tasks.add(task);
    listeners.forEach(l -> l.tasksChanged("Task " + task.getName() + " was placed in queue."));
    triggerTaskStart();
  }

  @Override
  public void receivedStatus(Client client, String status) {
    client.setStatus(status);
    listeners.forEach(l -> l.clientsChanged("Client " + client.getUuid().toString() + "send status " + status + "."));
  }

  /**
   * This method actually starts tasks if there is sufficient number of ready nodes.
   */
  private void triggerTaskStart() {
    synchronized (clientLock) {
      if (clients.size() == 0 || nextTask == tasks.size()) {
        return;
      }


      while (tasks.size() > nextTask) {
        if (clients.size() < tasks.get(nextTask).getNodes()) {
          tasks.get(nextTask).setState(TaskState.FAILED);
          tasks.get(nextTask).setResult("Failed because not enough nodes were connected.");
          listeners.forEach(l -> l.tasksChanged("Task " + tasks.get(nextTask).getName() + " failed because not enough nodes are connected."));
          nextTask++;
        } else if (tasks.get(nextTask).getNodes() <= getReadyClientsNumber()) {
          //  get a list of nodes we will be running our task on
          var targets = clients.stream()
              .filter(Client::isReady)
              .limit(tasks.get(nextTask).getNodes())
              .collect(Collectors.toList());
          //  create a new start task DTO
          var startTask = new StartTask(
              tasks.get(nextTask).getFullName(),
              tasks.get(nextTask).getParameters(),
              targets.stream().map(t -> t.getUuid().toString()).collect(Collectors.toList()).toArray(String[]::new));
          //  add files to storage
          AbstractAlgorithm algorithm;
          try {
            algorithm = (AbstractAlgorithm) Class.forName(tasks.get(nextTask).getFullName()).getDeclaredConstructor().newInstance();
            algorithm.getDistributedFiles().forEach((id, file) -> {
              try {
                Distributor.getDistributor().addDataSource(id, file);
              } catch (NoDataServerIsRunning e) {
                LOGGER.severe(e.getMessage());
              }
            });
            tasks.get(nextTask).setAlgorithm(algorithm);
          } catch (ClassNotFoundException | NoSuchMethodException | InstantiationException | IllegalAccessException |
                   InvocationTargetException ex) {
            LOGGER.severe(ex.getMessage());
          }

          new Thread(() -> tasks.get(nextTask).getAlgorithm().serverPart(this, targets.stream().map(t -> t.getUuid().toString()).toArray(String[]::new), tasks.get(nextTask).getParameters())).start();
          //  send message and start each target node
          targets.forEach(t -> {
            t.setTask(tasks.get(nextTask));
            t.setClientState(ClientState.COMPUTING);
            t.send(new Message("server", t.getUuid().toString(), gson.toJson(startTask), MessageType.START));
          });
          //  update task data and notify listeners
          tasks.get(nextTask).setState(TaskState.RUNNING);
          tasks.get(nextTask).setStartTime();
          listeners.forEach(l -> l.tasksChanged("Task " + tasks.get(nextTask).getName() + " started."));

          nextTask++;
        } else {
          break;
        }
      }
    }
  }

  /**
   * Returns number of ready nodes connected to the server.
   *
   * @return Number of ready nodes.
   */
  private long getReadyClientsNumber() {
    return clients.stream().filter(Client::isReady).count();
  }

  /**
   * Stops selected task. Does nothing when there is no task running.
   */
  public void stopTask(Task task) {
    synchronized (clientLock) {
      if (task.getState() == TaskState.RUNNING) {
        clients.stream().filter(c -> c.getTask() == task).forEach(c -> {
          c.send(new Message("server", c.getUuid().toString(), "", MessageType.STOP));
          c.setClientState(ClientState.STOPPING);
        });
        task.setState(TaskState.STOPPED);
        listeners.forEach(l -> l.clientsChanged("Stopping clients working on " + task.getName()));
        listeners.forEach(l -> l.tasksChanged("Task " + task.getName() + " stopped."));
      }
    }
  }

  /**
   * Sends information about all connected clients to all connected clients.
   */
  private void sendClientsInfo() {
    var dtos = clients.stream()
        .filter(c -> c.isReady() || c.isComputing())
        .map(Client::toNodeDto)
        .collect(Collectors.toList());
    clients.stream()
        .filter(c -> c.isReady() || c.isComputing())
        .forEach(c -> c.send(new Message(
            "server",
            c.getUuid().toString(),
            gson.toJson(dtos.toArray(new NodeDto[0])),
            MessageType.NODES
        )));
  }

  /**
   * Called when a message should be logged.
   *
   * @param message Mesage to log.
   */
  public void logMessage(String message) {
    listeners.forEach(l -> l.logMessage(message));
  }

  /**
   * Suspend the given client. Client must be in ready state.
   *
   * @param client Client to suspend.
   */
  public void suspendClient(Client client) {
    if (client.isReady()) {
      synchronized (clientLock) {
        client.setClientState(ClientState.SUSPENDED);
        listeners.forEach(l -> l.clientsChanged("Suspended node " + client.getUuid().toString()));
      }
    }
  }

  /**
   * Unsuspend client.
   *
   * @param client Client to unsuspend.
   */
  public void unsuspendClient(Client client) {
    if (client.isSuspended()) {
      synchronized (clientLock) {
        client.setClientState(ClientState.READY);
        listeners.forEach(l -> l.clientsChanged("Unsuspended node " + client.getUuid().toString()));
        triggerTaskStart();
      }
    }
  }

  @Override
  public void finishedTask(Client client, String partialResult) {
    synchronized (clientLock) {
      if (client.isStopping()) {
        stoppedTask(client);
        return;
      }
      if (!client.isComputing()) {
        return;
      }

      var task = client.getTask();

      task.addPartialResult(client.getAddress().getHostAddress(), partialResult);
      client.setTask(null);
      client.setClientState(ClientState.READY);
      client.setStatus("");

      listeners.forEach(l -> l.clientsChanged("Node " + client.getUuid().toString() + " finished task " + task.getName() + "with partial result " + partialResult + "."));

      var count = clients.stream().filter(c -> c.getTask() == task).count();

      if (count == 0) {
        AbstractAlgorithm algorithm;
        try {
          algorithm = (AbstractAlgorithm) Class.forName(task.getFullName()).getDeclaredConstructor().newInstance();
        } catch (ClassNotFoundException | NoSuchMethodException | InstantiationException | IllegalAccessException |
                 InvocationTargetException ex) {
          LOGGER.severe(ex.getMessage());
          return;
        }
        String result = algorithm.composeResult(task.getPartialResults().toArray(String[]::new));
        LOGGER.info(() -> "Finished " + task.getName() + " with result " + result + ".");
        task.setResult(result);
        task.setState(TaskState.FINISHED);
        task.setEndTime();
        listeners.forEach(l -> l.tasksChanged("Task " + task.getName() + " finished."));
      }

      triggerTaskStart();
    }
  }

  @Override
  public void failedTask(Client client, String cause) {
    synchronized (clientLock) {
      if (!client.isComputing()) {
        return;
      }
      var task = client.getTask();
      client.setTask(null);
      client.setClientState(ClientState.READY);
      client.setStatus("");
      clients.stream()
          .filter(c -> c.getTask() == task)
          .forEach(c -> {
            c.send(new Message("server", c.getUuid().toString(), "", MessageType.STOP));
            c.setClientState(ClientState.STOPPING);
          });
      task.setEndTime();
      task.setState(TaskState.FAILED);
      task.setResult("Failed due to: " + cause);

      LOGGER.warning(() -> "Task " + task.getName() + " failed due to " + cause + ".");

      listeners.forEach(l -> l.tasksChanged("Task " + task.getName() + " failed."));
      listeners.forEach(l -> l.clientsChanged("Client " + client.getUuid().toString() + " failed the task " + task.getName() + " due to " + cause));

      triggerTaskStart();
    }
  }

  @Override
  public void stoppedTask(Client client) {
    synchronized (clientLock) {
      if (!client.isStopping()) {
        return;
      }
      client.setTask(null);
      client.setClientState(ClientState.READY);
      client.setStatus("");

      listeners.forEach(l -> l.clientsChanged("Client " + client.getUuid().toString() + " stopped."));

      triggerTaskStart();
    }
  }

  @Override
  public void receivedError(Client client, String description) {
    listeners.forEach(l -> l.receivedError(client, description));
  }

  public void setUdpPort(int port, Client client) {
    client.setUdpPort(port);
    LOGGER.info(() -> "Set " + client + " UDP port to " + port + ".");
    listeners.forEach(l -> l.logMessage("Set " + client + " UDP port to " + port + "."));
  }

  public void relayMessage(String uuid, Message message) throws RecepientDoesNotExist {
    Optional<Client> recipient;
    try {
      recipient = clients.stream().filter(c -> c.getUuid().equals(UUID.fromString(uuid))).findFirst();
    } catch (IllegalArgumentException ex) {
      throw new RecepientDoesNotExist();
    }
    if (recipient.isPresent()) {
      recipient.get().send(message);
      LOGGER.info(() -> "Relayed message from " + message.getFrom() + " to " + message.getTo() + ".");
    } else {
      LOGGER.warning(() -> "Relay from " + message.getFrom() + " to " + message.getTo() + " was unsuccessful. Recipient does not exist.");
      throw new RecepientDoesNotExist();
    }
  }

  @Override
  public void sendMessageToNode(String uuid, String content) {
    Optional<Client> client = clients.stream().filter(c -> c.getUuid().equals(UUID.fromString(uuid))).findFirst();
    if (client.isPresent()) {
      client.get().send(new Message("server", uuid, content, MessageType.SERVER_TO_NODE));
    }
  }

  @Override
  public InetAddress getAddressForUuid(String uuid) {
    return clients.stream().filter(c -> c.getUuid().toString().equals(uuid)).findFirst().get().getAddress();
  }
}