package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.server.handlers;

import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.Message;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.MessageType;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.server.Client;

/**
 * Handler for ERROR messages.
 *
 * @author Roman Janků (jankurom@fel.cvut.cz)
 */
public class NodeToServerHandler extends AbstractHandler {

  public NodeToServerHandler() {
    handledMessageType = MessageType.NODE_TO_SERVER;
  }

  @Override
  public void handleMessage(Message message, Client client) {
    client.getTask().getAlgorithm().serverReceivedMessageFromNode(message.getContent(), message.getFrom());
  }
}
