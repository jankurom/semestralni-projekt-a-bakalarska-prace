package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.listeners;

import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.server.Client;

/**
 * This listener listens to a server for information on changes weather the server is running.
 *
 * @author Roman Janků (jankurom@fel.cvut.cz)
 */
public interface ServerListener {
  /**
   * This event is generated when server starts.
   */
  void serverStarted();

  /**
   * This event is generated when server stops.
   */
  void serverStopped();

  /**
   * This event is generated when client list change or the data of one or more clients change.
   *
   * @param message Message to print.
   */
  void clientsChanged(String message);

  /**
   * This event is generated when task list or data inside the tasks has changed.
   *
   * @param message Message to print.
   */
  void tasksChanged(String message);

  /**
   * Triggered when server receives error.
   *
   * @param client      Node from which error was received.
   * @param description Description of the error.
   */
  void receivedError(Client client, String description);

  /**
   * Called when server wants to log message.
   *
   * @param message Message to log.
   */
  void logMessage(String message);
}