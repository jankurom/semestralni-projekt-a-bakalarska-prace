package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.gui;

import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.exporters.JsonExporter;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.importers.TaskImporter;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.listeners.ServerListener;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.server.Client;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.server.Server;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.server.Task;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 * Main window of the server application.
 *
 * @author Roman Janků (jankurom@fel.cvut.cz)
 */
public class MainWindow extends JFrame implements ServerListener {

  //  Elements of gui that are dynamically changed during runtime.
  JList<Client> nodes;
  JList<Task> tasks;
  JTextArea logs;
  JLabel clientAddress;
  JLabel clientUUID;
  JLabel clientState;
  JLabel clientTask;
  JLabel clientTaskStatus;
  JLabel taskName;
  JLabel taskStatus;
  JLabel taskTime;
  JLabel taskNodes;
  JLabel taskResult;
  JButton kick;
  JButton suspend;
  JButton unsuspend;

  /**
   * Creates a new main window.
   */
  public MainWindow() {
    setExtendedState(getExtendedState() | JFrame.MAXIMIZED_BOTH);
    setMinimumSize(new Dimension(600, 400));
    setTitle("Server for PicoCluster distributed algorithm");
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    buildGui();

    Server.getInstance().addListener(this);

    setVisible(true);
  }

  /**
   * Method for building the gui of the main window.
   */
  private void buildGui() {
    var splitPaneHorizontal = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, buildTopLeft(), buildTopRight());
    var splitPaneVertical = new JSplitPane(JSplitPane.VERTICAL_SPLIT, splitPaneHorizontal, buildBottom());
    getContentPane().add(splitPaneVertical);
  }

  /**
   * Method for building clients list and action buttons.
   *
   * @return Panel with clients list and action buttons.
   */
  private JPanel buildTopLeft() {
    var panel = new JPanel();
    panel.setLayout(new BorderLayout());

    nodes = new JList<>();
    nodes.addListSelectionListener(arg0 -> changedClient());
    nodes.setMinimumSize(new Dimension(250, 400));
    nodes.setListData(Server.getInstance().getClients());

    JScrollPane scroll = new JScrollPane(nodes);
    scroll.setMinimumSize(new Dimension(250, 400));

    panel.add(new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, scroll, buildNodesInfo()));

    return panel;
  }

  /**
   * Builds grid with node's info.
   *
   * @return JPanel with node's info.
   */
  private JPanel buildNodesInfo() {
    var panel = new JPanel();
    panel.setMinimumSize(new Dimension(400, 400));
    panel.setLayout(new GridBagLayout());

    var c = new GridBagConstraints();

    var buttonPanel = new JPanel();
    buttonPanel.setLayout(new FlowLayout());
    kick = new JButton("Kick");
    kick.addActionListener(e -> clickedKick());
    buttonPanel.add(kick);

    suspend = new JButton("Suspend");
    suspend.addActionListener(e -> clickedSuspend());
    buttonPanel.add(suspend);

    unsuspend = new JButton("Wake");
    unsuspend.addActionListener(e -> clickedUnsuspend());
    buttonPanel.add(unsuspend);

    c.gridx = 0;
    c.gridy = 0;
    c.gridwidth = 2;
    panel.add(buttonPanel, c);

    c.gridy = 1;
    c.gridwidth = 1;
    c.weightx = 0;
    c.weighty = 1;
    panel.add(new JLabel("IP address:"), c);

    c.gridy = 2;
    panel.add(new JLabel("UUID:"), c);

    c.gridy = 3;
    panel.add(new JLabel("State:"), c);

    c.gridy = 4;
    panel.add(new JLabel("Task:"), c);

    c.gridy = 5;
    panel.add(new JLabel("Task status:"), c);

    clientAddress = new JLabel("<ADDRESS>");
    c.gridx = 1;
    c.gridy = 1;
    c.weightx = 1;
    panel.add(clientAddress, c);

    clientUUID = new JLabel("<UUID>");
    c.gridy = 2;
    panel.add(clientUUID, c);

    clientState = new JLabel("<STATE>");
    c.gridy = 3;
    panel.add(clientState, c);

    clientTask = new JLabel("<TASK>");
    c.gridy = 4;
    panel.add(clientTask, c);

    clientTaskStatus = new JLabel("<TASK_STATUS>");
    c.gridy = 5;
    panel.add(clientTaskStatus, c);

    return panel;
  }

  /**
   * Method for building panel with information about tasks
   *
   * @return Panel with information about tasks.
   */
  private JPanel buildTopRight() {
    var panel = new JPanel();
    panel.setLayout(new BorderLayout());

    tasks = new JList<>();
    tasks.addListSelectionListener(arg0 -> changedTask());
    tasks.setMinimumSize(new Dimension(250, 400));

    JScrollPane scroll = new JScrollPane(tasks);
    scroll.setMinimumSize(new Dimension(250, 400));

    panel.add(new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, scroll, buildTaskInfo()));

    return panel;
  }

  /**
   * Builds panel with task details.
   *
   * @return JPanel with task details.
   */
  private JPanel buildTaskInfo() {
    var panel = new JPanel();
    panel.setMinimumSize(new Dimension(400, 400));
    panel.setLayout(new GridBagLayout());

    var c = new GridBagConstraints();

    var buttonPanel = new JPanel();
    buttonPanel.setLayout(new FlowLayout());
    var add = new JButton("Add");
    add.addActionListener(e -> clickedAdd());
    buttonPanel.add(add);

    buttonPanel.setLayout(new FlowLayout());
    var remove = new JButton("Stop");
    remove.addActionListener(e -> clickedRemove());
    buttonPanel.add(remove);
    buttonPanel.setLayout(new FlowLayout());

    var export = new JButton("Export");
    export.addActionListener(e -> clickedExport());
    buttonPanel.add(export);

    var load = new JButton("Load tasks");
    load.addActionListener(e -> clickedLoad());
    buttonPanel.add(load);

    c.gridx = 0;
    c.gridy = 0;
    c.gridwidth = 2;
    panel.add(buttonPanel, c);

    c.gridy = 1;
    c.gridwidth = 1;
    c.weightx = 0;
    c.weighty = 0;
    panel.add(new JLabel("Name:"), c);

    c.gridy = 2;
    panel.add(new JLabel("Status:"), c);

    c.gridy = 3;
    panel.add(new JLabel("Time:"), c);

    c.gridy = 4;
    panel.add(new JLabel("Nodes:"), c);

    c.gridy = 5;
    c.weighty = 1;
    panel.add(new JLabel("Result:"), c);

    taskName = new JLabel("<NAME>");
    c.gridx = 1;
    c.gridy = 1;
    c.weightx = 1;
    panel.add(taskName, c);

    taskStatus = new JLabel("<STATUS>");
    c.gridy = 2;
    panel.add(taskStatus, c);

    taskTime = new JLabel("<TIME>");
    c.gridy = 3;
    panel.add(taskTime, c);

    taskNodes = new JLabel("<NODES>");
    c.gridy = 4;
    panel.add(taskNodes, c);

    taskResult = new JLabel("<RESULT>");
    c.gridy = 5;
    panel.add(taskResult, c);

    return panel;
  }

  /**
   * Method for building log text area.
   *
   * @return Panel with log text area.
   */
  private JPanel buildBottom() {
    var panel = new JPanel();
    panel.setLayout(new BoxLayout(panel, BoxLayout.LINE_AXIS));

    logs = new JTextArea();
    logs.setEditable(false);
    logs.setBorder(BorderFactory.createCompoundBorder(logs.getBorder(), BorderFactory.createEmptyBorder(5, 5, 5, 5)));
    panel.add(new JScrollPane(logs));

    return panel;
  }

  /**
   * Write a log line to the log text area. Automatically adds date and time.
   *
   * @param line Writes a line to the log text area.
   */
  private synchronized void logsWrite(String line) {
    logs.append("[" + LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd, HH:mm:ss")) + "]: " + line + "\n");
  }

  @Override
  public void serverStarted() {
    logsWrite("Server started.");
  }

  @Override
  public void serverStopped() {
    logsWrite("Server stopped.");
  }

  @Override
  public void clientsChanged(String message) {
    nodes.setListData(Server.getInstance().getClients());
    logsWrite(message);
  }

  @Override
  public void tasksChanged(String message) {
    tasks.setListData(Server.getInstance().getTasks());
    logsWrite(message);
  }

  @Override
  public void receivedError(Client client, String description) {
    logsWrite("Error received from " + client + ". Content: " + description);
  }

  /**
   * User clicked on the task start button.
   */
  private void clickedAdd() {
    new TaskWindow();
  }

  @Override
  public void logMessage(String message) {
    logsWrite(message);
  }

  /**
   * User clicked on the stop task button.
   */
  private void clickedRemove() {
    if (tasks.getSelectedIndex() >= 0) {
      Server.getInstance().stopTask(tasks.getSelectedValue());
    } else {
      JOptionPane.showMessageDialog(this, "No task selected! Please select task.", "Cannot remove task!", JOptionPane.WARNING_MESSAGE);
    }
  }

  /**
   * User clicked on kick client button.
   */
  private void clickedKick() {
    if (nodes.getSelectedIndex() >= 0) {
      Server.getInstance().kickClient(nodes.getSelectedValue());
    } else {
      JOptionPane.showMessageDialog(this, "No node selected! Please node task.", "Cannot kick node!", JOptionPane.WARNING_MESSAGE);
    }
  }

  /**
   * User clicked on export button.
   */
  private void clickedExport() {
    var chooser = new JFileChooser();
    chooser.setDialogTitle("Select where to save");
    chooser.addChoosableFileFilter(new FileNameExtensionFilter("JSON object", "json"));
    if (chooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
      var exporter = new JsonExporter();
      try {
        exporter.export(Server.getInstance().getTasks(), chooser.getSelectedFile());
      } catch (IOException ex) {
        JOptionPane.showMessageDialog(this, "Could not save the file!\nCause: " + ex.getMessage(), "Could not save the file!", JOptionPane.ERROR_MESSAGE);
      }
    }
  }

  private void clickedLoad() {
    var chooser = new JFileChooser();
    chooser.setDialogTitle("Select file to load tasks from");
    chooser.addChoosableFileFilter(new FileNameExtensionFilter("JSON object", "json"));
    if (chooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
      try {
        TaskImporter.importFromFile(chooser.getSelectedFile().getAbsolutePath()).forEach(task -> Server.getInstance().addTask(task.getName(), task.getFullName(), task.getParameters(), task.getNodeCount()));
      } catch (IOException ex) {
        JOptionPane.showMessageDialog(this, "Could not save the file!\nCause: " + ex.getMessage(), "Could not save the file!", JOptionPane.ERROR_MESSAGE);
      }
    }
  }

  private void clickedSuspend() {
    if (nodes.getSelectedIndex() >= 0) {
      var node = nodes.getSelectedValue();
      if (node.isReady()) {
        Server.getInstance().suspendClient(node);
      }
    }
  }

  private void clickedUnsuspend() {
    if (nodes.getSelectedIndex() >= 0) {
      var node = nodes.getSelectedValue();
      if (node.isSuspended()) {
        Server.getInstance().unsuspendClient(node);
      }
    }
  }

  /**
   * User selected different client in client list.
   */
  private void changedClient() {
    if (nodes.getSelectedIndex() >= 0) {
      var client = nodes.getSelectedValue();
      clientUUID.setText(client.getUuid().toString());
      clientAddress.setText(client.getAddress().toString());
      clientState.setText(client.getClientState().toString());
      clientTask.setText((client.getTask() == null) ? "None" : client.getTask().getName());
      clientTaskStatus.setText(client.getStatus());

      if (client.isSuspended()) {
        unsuspend.setEnabled(true);
        suspend.setEnabled(false);
      } else {
        unsuspend.setEnabled(false);
        suspend.setEnabled(true);
      }
      kick.setEnabled(true);

    } else {
      clientUUID.setText("<UUID>");
      clientAddress.setText("<ADDRESS>");
      clientState.setText("<STATUS>");
      clientTask.setText("<TASK>");
      clientTaskStatus.setText("<TASK_STATUS>");

      suspend.setEnabled(false);
      unsuspend.setEnabled(false);
      kick.setEnabled(false);
    }
  }

  /**
   * User selected a different task.
   */
  private void changedTask() {
    if (tasks.getSelectedIndex() >= 0) {
      var task = tasks.getSelectedValue();
      taskName.setText(task.getName());
      taskStatus.setText(task.getState().toString());
      taskTime.setText(task.getEnlapsedTime().format(DateTimeFormatter.ISO_TIME));
      taskNodes.setText(Integer.toString(task.getNodes()));
      taskResult.setText(task.getResult());
    } else {
      taskName.setText("<NAME>");
      taskStatus.setText("<STATUS>");
      taskTime.setText("<TIME>");
      taskNodes.setText("<NODES>");
      taskResult.setText("<RESULT>");
    }
  }
}