package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.exceptions;

/**
 * Exception thrown by a Configuration class when it cannot load the configuration. This exception is fatal and server cannot continue after this exception is thrown.
 *
 * @author Roman Janků (jankurom@fel.cvut.cz)
 */
public class ConfigurationException extends RuntimeException {
  public ConfigurationException() {
  }

  public ConfigurationException(String message) {
    super(message);
  }

  public ConfigurationException(String message, Throwable cause) {
    super(message, cause);
  }
}
