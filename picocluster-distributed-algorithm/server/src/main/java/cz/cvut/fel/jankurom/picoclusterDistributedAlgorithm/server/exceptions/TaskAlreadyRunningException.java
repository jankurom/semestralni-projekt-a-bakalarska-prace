package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.exceptions;

/**
 * Thrown when user starts to run new task but there is one already running.
 *
 * @author Roman Janků (jankurom@fel.cvut.cz)
 */
public class TaskAlreadyRunningException extends Exception {
  public TaskAlreadyRunningException() {
  }

  public TaskAlreadyRunningException(String message) {
    super(message);
  }
}
