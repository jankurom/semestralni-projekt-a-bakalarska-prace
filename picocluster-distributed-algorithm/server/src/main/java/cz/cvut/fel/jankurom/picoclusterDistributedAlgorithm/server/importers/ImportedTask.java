package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.importers;

public class ImportedTask {
  private final String name;
  private final String fullName;
  private final String[] parameters;
  private final int nodeCount;

  public ImportedTask(String name, String fullName, String[] parameters, int nodeCount) {
    this.name = name;
    this.fullName = fullName;
    this.parameters = parameters;
    this.nodeCount = nodeCount;
  }

  public String getName() {
    return name;
  }

  public String getFullName() {
    return fullName;
  }

  public String[] getParameters() {
    return parameters;
  }

  public int getNodeCount() {
    return nodeCount;
  }
}
