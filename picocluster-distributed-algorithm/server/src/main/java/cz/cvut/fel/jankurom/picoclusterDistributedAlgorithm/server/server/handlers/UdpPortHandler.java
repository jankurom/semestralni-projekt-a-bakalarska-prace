package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.server.handlers;

import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.Message;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.MessageType;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.server.Client;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.server.Server;

/**
 * Handler for ERROR messages.
 *
 * @author Roman Janků (jankurom@fel.cvut.cz)
 */
public class UdpPortHandler extends AbstractHandler {

  public UdpPortHandler() {
    handledMessageType = MessageType.UDP_PORT;
  }

  @Override
  public void handleMessage(Message message, Client client) {
    Server.getInstance().setUdpPort(Integer.parseInt(message.getContent()), client);
  }
}
