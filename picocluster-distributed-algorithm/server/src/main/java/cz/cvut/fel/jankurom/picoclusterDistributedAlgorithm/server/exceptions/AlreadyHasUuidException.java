package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.exceptions;

/**
 * Thrown when node has an UUID set and it tries to set a new one.
 *
 * @author Roman Janků (jankurom@fel.cvut.cz)
 */
public class AlreadyHasUuidException extends Exception {
  public AlreadyHasUuidException() {
  }

  public AlreadyHasUuidException(String message) {
    super(message);
  }
}
