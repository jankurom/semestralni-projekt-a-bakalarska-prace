package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.server;

/**
 * Representation of the client state. See client state diagram and documentation for more information.
 *
 * @author Roman Janků
 */
public enum ClientState {
  /**
   * The client is connected but cannot receive problem for computing yet.
   */
  CONNECTED("CONNECTED"),
  /**
   * Client is ready for computing.
   */
  READY("READY"),
  /**
   * Client is computing.
   */
  COMPUTING("COMPUTING"),
  /**
   * Client is stopping the computation.
   */
  STOPPING("STOPPING"),
  /**
   * Client is disconnected and will be deleted in near future.
   */
  DISCONNECTED("DISCONNECTED"),
  /**
   * Client is in suspended state and is not participating in computations.
   */
  SUSPENDED("SUSPENDED");

  private final String name;

  ClientState(String name) {
    this.name = name;
  }

  @Override
  public String toString() {
    return name;
  }
}