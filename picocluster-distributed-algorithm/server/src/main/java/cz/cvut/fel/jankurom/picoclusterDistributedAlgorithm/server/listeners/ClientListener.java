package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.listeners;

import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.server.Client;

/**
 * Defines listener that listens to events triggered by a client.
 *
 * @author Roman Janků (jankurom@fel.cvut.cz)
 */
public interface ClientListener {
  /**
   * Called by client when receives FINISHED message.
   *
   * @param client Client that have finished.
   * @param result Task from the client.
   */
  void finishedTask(Client client, String result);

  /**
   * Called by client when receives FAILED message.
   *
   * @param client Client that failed.
   * @param cause  The reason the client failed.
   */
  void failedTask(Client client, String cause);

  /**
   * Called when client receives an ERROR message
   *
   * @param client      Client from which the ERROR message was received.
   * @param description Description in the error message.
   */
  void receivedError(Client client, String description);

  /**
   * Called when client receives STATUS message.
   *
   * @param client Client that received status message.
   * @param status Status message.
   */
  void receivedStatus(Client client, String status);

  /**
   * Called when received STOPPED message from client.
   *
   * @param client Client that has stopped.
   */
  void stoppedTask(Client client);
}