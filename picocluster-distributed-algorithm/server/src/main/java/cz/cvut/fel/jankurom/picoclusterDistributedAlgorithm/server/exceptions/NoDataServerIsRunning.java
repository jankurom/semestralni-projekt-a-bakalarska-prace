package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.exceptions;

/**
 * Thrown when there is no data server running.
 */
public class NoDataServerIsRunning extends Exception {
}