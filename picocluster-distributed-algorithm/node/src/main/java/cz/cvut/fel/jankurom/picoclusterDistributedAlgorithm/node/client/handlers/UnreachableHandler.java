package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.node.client.handlers;

import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.Message;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.MessageType;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.node.Main;
import java.util.logging.Logger;

/**
 * Handler for handling ERROR messages.
 *
 * @author Roman Janků (jankurom@fel.cvut.cz)
 */
public class UnreachableHandler extends AbstractHandler {

  private static final Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

  /**
   * Create a new StartHandler.
   */
  public UnreachableHandler() {
    handledMessageType = MessageType.UNREACHABLE;
  }

  @Override
  public void handleMessage(Message message) {
    Main.getClient().nodeUnreachable(message.getContent());
  }
}