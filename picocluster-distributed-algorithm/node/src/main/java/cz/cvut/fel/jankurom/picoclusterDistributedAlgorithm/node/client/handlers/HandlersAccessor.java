package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.node.client.handlers;

import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.MessageType;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.exceptions.NoHandlerForMessageTypeException;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.logging.Logger;
import org.reflections.Reflections;

/**
 * Class for accessing handlers for messages.
 *
 * @author Roman Janků (jankurom@fel.cvut.cz)
 */
public final class HandlersAccessor {

  private static final Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
  /**
   * Reflections for accessing and loading the classes.
   */
  private static final Reflections reflections;
  /**
   * List of all loaded handlers.
   */
  private static final Map<MessageType, AbstractHandler> handlers;

  static {
    reflections = new Reflections("cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.node.client.handlers");
    handlers = new HashMap<>();
    reflections.getSubTypesOf(AbstractHandler.class).stream()
        .map(h -> {
          try {
            return h.getDeclaredConstructor().newInstance();
          } catch (InstantiationException | IllegalAccessException | InvocationTargetException |
                   NoSuchMethodException ex) {
            LOGGER.warning(() -> "Could not load '" + h.getName() + "'. Cause: " + ex.getMessage());
          }
          return null;
        })
        .filter(Objects::nonNull)
        .forEach(h -> {
          if (!handlers.containsKey(h.getHandledMessageType())) {
            handlers.put(h.getHandledMessageType(), h);
          }
        });
  }

  private HandlersAccessor() {

  }

  /**
   * Returns handler for given MessageType or null if there is no handler for given MessageType.
   *
   * @param messageType MessageType to look for.
   * @return handler for MessageType or null.
   */
  public static AbstractHandler getHandlerForMessageType(MessageType messageType) throws NoHandlerForMessageTypeException {
    if (hasHandlerForMessageType(messageType)) {
      return handlers.get(messageType);
    }
    throw new NoHandlerForMessageTypeException("No handler for '" + messageType + "' type of message.");
  }

  /**
   * Tells weather there is handler for given MessageType.
   *
   * @param messageType MessageType to look for.
   * @return TRUE if there is handler for given MessageType, FALSE otherwise.
   */
  public static boolean hasHandlerForMessageType(MessageType messageType) {
    return handlers.containsKey(messageType);
  }
}