package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.node.client.handlers;

import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.Message;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.MessageType;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.node.Main;

/**
 * Handler for handling STOP messages.
 *
 * @author Roman Janků (jankurom@fel.cvut.cz)
 */
public class StopHandler extends AbstractHandler {

  /**
   * Create a new StartHandler.
   */
  public StopHandler() {
    handledMessageType = MessageType.STOP;
  }

  @Override
  public void handleMessage(Message message) {
    Main.getClient().stopTask();
  }
}
