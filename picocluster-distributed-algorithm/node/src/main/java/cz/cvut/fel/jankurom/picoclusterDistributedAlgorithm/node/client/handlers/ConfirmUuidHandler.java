package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.node.client.handlers;

import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.Message;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.MessageType;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.node.Main;
import java.io.IOException;
import java.util.UUID;

/**
 * Handler for handling UUID_CONFIRM messages. It simply assigns UUID if the node does not have one.
 *
 * @author Roman Janků (jankurom@fel.cvut.cz)
 */
public class ConfirmUuidHandler extends AbstractHandler {

  /**
   * Creates a new handler.
   */
  public ConfirmUuidHandler() {
    handledMessageType = MessageType.UUID_CONFIRM;
  }

  @Override
  public void handleMessage(Message message) {
    if (!message.getFrom().equals("server")) {
      LOGGER.warning(() -> message.getFrom() + " tried setting UUID although he is not server!");
      try {
        Main.getClient().sendMessage(new Message(
            Main.getClient().getUuid().toString(),
            message.getFrom(),
            "Only server can confirm UUID!",
            MessageType.ERROR
        ));
      } catch (IOException ex) {
        LOGGER.severe(() -> "Could not send ERROR message to client.");
      }
    } else if (Main.getClient().hasUuid()) {
      LOGGER.warning(() -> "Server trying to set UUID when already set.");
      try {
        Main.getClient().sendMessage(new Message(
            Main.getClient().getUuid().toString(),
            "server",
            "Already have an UUID!",
            MessageType.ERROR
        ));
      } catch (IOException ex) {
        LOGGER.severe(() -> "Could not send ERROR message to client.");
      }
    } else if (!message.getContent().equals("")) {
      Main.getClient().setUuid(UUID.fromString(message.getContent()));
      LOGGER.info(() -> "Set UUID to " + message.getContent() + ".");
    }
  }
}
