package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.node.client;

import java.awt.Color;
import jimbo.pijava.blinkt.BlinktController;

/**
 * Controller for Blinkt! LEDs
 *
 * @author Roman Janků (jankurom@fel.cvut.cz)
 */
public final class BlinkIt {

  private static final BlinktController blinkIt;
  private static Color[] backup;
  private static volatile boolean custom;

  static {
    blinkIt = new BlinktController();
  }

  private BlinkIt() {

  }

  /**
   * Initializes the LEDs to initial state.
   */
  public static void initialise() {
    backup = new Color[8];
    custom = false;
    blinkIt.clear();
    blinkIt.setBrightness(.1F);
    blinkIt.set(0, Color.GREEN);
    backup[0] = Color.GREEN;
    blinkIt.push();
  }

  /**
   * Sets LEDs to connected state.
   */
  public static void setConnected() {
    restore();
    for (int i = 2; i < 8; i++) {
      turnOf(i);
    }
    blinkIt.set(1, Color.GREEN);
    backup[1] = Color.GREEN;
    blinkIt.push();
  }

  /**
   * Sets LEDs to ready state.
   */
  public static void setReady() {
    restore();
    for (int i = 3; i < 8; i++) {
      turnOf(i);
    }
    blinkIt.set(2, Color.GREEN);
    backup[2] = Color.GREEN;
    blinkIt.push();
  }

  /**
   * Sets LEDs to computing state.
   */
  public static void setComputing() {
    restore();
    turnOf(5);
    turnOf(6);
    blinkIt.set(7, Color.YELLOW);
    backup[7] = Color.YELLOW;
    blinkIt.push();
  }

  /**
   * Sets LEDs top finished state.
   */
  public static void setFinished() {
    restore();
    blinkIt.set(7, Color.GREEN);
    backup[7] = Color.GREEN;
    blinkIt.push();
  }

  /**
   * Sets LEDs to stopped state.
   */
  public static void setStopped() {
    restore();
    turnOf(7);
    blinkIt.set(6, Color.RED);
    backup[6] = Color.RED;
    blinkIt.push();
  }

  /**
   * Sets LEDs to failed state.
   */
  public static void setFailed() {
    restore();
    turnOf(7);
    blinkIt.set(5, Color.RED);
    backup[5] = Color.RED;
    blinkIt.push();
  }

  /**
   * Sets LEDs to disconnected state.
   */
  public static void setDisconnected() {
    restore();
    for (int i = 2; i < 8; i++) {
      turnOf(i);
    }
    blinkIt.set(1, Color.YELLOW);
    backup[1] = Color.YELLOW;
    blinkIt.push();
  }

  /**
   * Sets LEDs to terminated state.
   */
  public static void setTerminated() {
    restore();
    for (int i = 0; i < 8; i++) {
      turnOf(i);
    }
    blinkIt.set(3, Color.RED);
    blinkIt.set(4, Color.RED);
    backup[3] = Color.RED;
    backup[4] = Color.RED;
    blinkIt.push();
  }

  /**
   * Sets selected LED to desired color.
   *
   * @param index Index of the LED.
   * @param color Color of the LED.
   */
  public static void setCustom(int index, Color color) {
    backUp();
    blinkIt.set(index, color);
    blinkIt.push();
  }

  /**
   * Sets LED at selected index to custom color.
   *
   * @param index Index of the LED.
   * @param red   Red part of the color.
   * @param green Green part of the color.
   * @param blue  Blue part of the color.
   */
  public static void setCustom(int index, int red, int green, int blue) {
    backUp();
    blinkIt.set(index, red, green, blue);
    blinkIt.push();
  }

  /**
   * Sets brightness of all LEDs.
   *
   * @param brightness Sets brightness of LEDs.
   */
  public static void setBrightness(float brightness) {
    backUp();
    blinkIt.setBrightness(brightness);
    blinkIt.push();
  }

  private static void turnOf(int index) {
    blinkIt.set(index, 0, 0, 0);
    backup[index] = null;
    blinkIt.push();
  }

  private static void backUp() {
    if (custom) {
      return;
    }
    for (int i = 0; i < 8; i++) {
      blinkIt.set(i, 0, 0, 0);
    }
    custom = true;
    blinkIt.push();
  }

  private static void restore() {
    if (!custom) {
      return;
    }
    blinkIt.setBrightness(.1F);
    for (int i = 0; i < 8; i++) {
      if (backup[i] == null) {
        turnOf(i);
      } else {
        blinkIt.set(i, backup[i]);
      }
    }
    custom = false;
    blinkIt.push();
  }
}