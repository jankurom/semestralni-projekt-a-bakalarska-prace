package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.node.client;

import java.net.InetAddress;

public interface UdpListener {
  /**
   * Called when message is received over UDP.
   *
   * @param address Address of the sender.
   * @param content Content of the message.
   */
  void receivedUdpMessage(InetAddress address, String content);
}