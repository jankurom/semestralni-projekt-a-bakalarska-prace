package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.node.client.handlers;

import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.Message;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.MessageType;
import java.util.logging.Logger;

/**
 * Handler for handling ERROR messages.
 *
 * @author Roman Janků (jankurom@fel.cvut.cz)
 */
public class ErrorHandler extends AbstractHandler {

  private static final Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

  /**
   * Create a new StartHandler.
   */
  public ErrorHandler() {
    handledMessageType = MessageType.ERROR;
  }

  @Override
  public void handleMessage(Message message) {
    LOGGER.warning(() -> "Received error from " + message.getFrom() + ". Content: " + message.getContent());
  }
}