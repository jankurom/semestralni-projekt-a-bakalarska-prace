package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.node.client.handlers;

import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.Message;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.MessageType;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.node.Main;
import java.util.logging.Logger;

/**
 * Handler for handling ERROR messages.
 *
 * @author Roman Janků (jankurom@fel.cvut.cz)
 */
public class ServerToNodeHandler extends AbstractHandler {

  private static final Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

  /**
   * Create a new StartHandler.
   */
  public ServerToNodeHandler() {
    handledMessageType = MessageType.SERVER_TO_NODE;
  }

  @Override
  public void handleMessage(Message message) {
    Main.getClient().receivedMessageFromServer(message.getContent());
  }
}