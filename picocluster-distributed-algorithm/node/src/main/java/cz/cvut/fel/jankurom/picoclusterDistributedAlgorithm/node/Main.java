package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.node;

import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.node.client.BlinkIt;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.node.client.Client;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.logging.Logger;

/**
 * Main class of the node application with main method.
 *
 * @author Roman Janků (jankurom@fel.cvut.cz)
 */
public final class Main {
  private static final Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

  private static InetAddress serverAddress;
  private static int serverPort;
  private static int udpMaxPacketSize;
  private static boolean notifyServer;

  /**
   * Client that is connected to server.
   */
  private static Client client;

  private Main() {

  }

  /**
   * Main method of the node application.
   *
   * @param args Parameters from terminal.
   */
  public static void main(String... args) {
    BlinkIt.initialise();

    Runtime.getRuntime().addShutdownHook(new Thread(() -> BlinkIt.setTerminated()));

    LOGGER.info(() -> "Node application is starting ...");
    try {
      serverAddress = InetAddress.getByName(System.getenv("PICOCLUSTER_SERVER").split(":")[0]);
      serverPort = Integer.parseInt(System.getenv("PICOCLUSTER_SERVER").split(":")[1]);
      udpMaxPacketSize = Integer.parseInt(System.getenv("UDP_PACKET_SIZE"));
      notifyServer = (System.getenv("END_SENT_MESSAGE_INFO").equalsIgnoreCase("true"));

      client = new Client();
      client.start();
      BlinkIt.initialise();
      LOGGER.info(() -> "Node application has successfully started.");
    } catch (IOException ex) {
      LOGGER.severe(() -> "Problem happened when starting node. Will terminate. Cause: " + ex.getMessage());
    }
  }

  /**
   * Parses the address of the server from environmental variable containing server's IP address and port.
   *
   * @return Parsed IP address.
   * @throws UnknownHostException Thrown when IP address is unknown.
   */
  public static InetAddress getServerAddress() throws UnknownHostException {
    return serverAddress;
  }

  /**
   * Parses the port number of the server from environmental variable containing server's IP address and port.
   *
   * @return Parsed server port number.
   */
  public static int getServerPort() {
    return serverPort;
  }

  /**
   * Returns the maximum size of UDP packet in bytes.
   *
   * @return Maximum size of UDP packet.
   */
  public static int getMaxUdpPacketSize() {
    return udpMaxPacketSize;
  }

  /**
   * Tells weather server should be notified when message is sent on UDP.
   *
   * @return Notify server ond UDP sent.
   */
  public static boolean notifyServerOnMessageSent() {
    return notifyServer;
  }

  /**
   * Returns the Client class created by main method.
   *
   * @return Client class created by main method.
   */
  public static Client getClient() {
    return client;
  }
}