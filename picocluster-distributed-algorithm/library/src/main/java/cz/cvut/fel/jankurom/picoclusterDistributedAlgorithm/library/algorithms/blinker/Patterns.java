package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.blinker;

import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.TaskRunner;
import java.awt.Color;
import java.util.Random;
import java.util.concurrent.TimeUnit;

final class Patterns {

  private volatile boolean stop = false;
  private volatile boolean stopped = false;

  /**
   * Run selected pattern.
   *
   * @param pattern    Pattern to run.
   * @param taskRunner Runner of the task.
   */
  void runPattern(int pattern, TaskRunner taskRunner) {
    switch (pattern) {
      case 1:
        rgbSnake(taskRunner);
        break;
      case 2:
        rgbFade(taskRunner);
        break;
      case 3:
        rgbDisco(taskRunner);
        break;
      case 4:
        rgbRunner(taskRunner);
        break;
      default:
        stopped = true;
    }
  }

  void stop() {
    stop = true;
  }

  boolean isStopped() {
    return stopped;
  }

  private void rgbSnake(TaskRunner taskRunner) {
    int offset = 0;
    while (!stop) {
      taskRunner.setLed((offset) % 8, Color.RED);
      taskRunner.setLed((1 + offset) % 8, Color.ORANGE);
      taskRunner.setLed((2 + offset) % 8, Color.YELLOW);
      taskRunner.setLed((3 + offset) % 8, 170, 200, 66);
      taskRunner.setLed((4 + offset) % 8, Color.GREEN);
      taskRunner.setLed((5 + offset) % 8, 0, 255, 255);
      taskRunner.setLed((6 + offset) % 8, Color.BLUE);
      taskRunner.setLed((7 + offset) % 8, 255, 0, 255);
      offset++;
      offset = offset % 8;
      try {
        TimeUnit.MILLISECONDS.sleep(250);
      } catch (InterruptedException ex) {

      }
    }
    try {
      TimeUnit.MILLISECONDS.sleep(2000);
    } catch (InterruptedException ex) {

    }
    stopped = true;
  }

  private void rgbFade(TaskRunner runner) {
    int[] colors = {255, 0, 0};
    int index = 0;

    while (!stop) {
      for (int i = 0; i < 255; i++) {
        colors[index % 3] -= 1;
        colors[(index + 1) % 3] += 1;

        for (int j = 0; j < 8; j++) {
          runner.setLed(j, colors[0], colors[1], colors[2]);
        }

        try {
          TimeUnit.MILLISECONDS.sleep(10);
        } catch (InterruptedException ex) {

        }
      }
      index = (index + 1) % 3;
    }
    try {
      TimeUnit.MILLISECONDS.sleep(2000);
    } catch (InterruptedException ex) {

    }
    stopped = true;
  }

  private void rgbDisco(TaskRunner runner) {
    Random rand = new Random();
    int[] colors = new int[3];
    while (!stop) {
      colors[0] = rand.nextInt(256);
      colors[1] = rand.nextInt(256);
      colors[2] = rand.nextInt(256);

      for (int j = 0; j < 8; j++) {
        runner.setLed(j, colors[0], colors[1], colors[2]);
      }

      try {
        TimeUnit.MILLISECONDS.sleep(250);
      } catch (InterruptedException ex) {

      }

      for (int j = 0; j < 8; j++) {
        runner.setLed(j, 0, 0, 0);
      }

      try {
        TimeUnit.MILLISECONDS.sleep(250);
      } catch (InterruptedException ex) {

      }
    }

    try {
      TimeUnit.MILLISECONDS.sleep(2000);
    } catch (InterruptedException ex) {

    }
  }

  private void rgbRunner(TaskRunner runner) {
    int index = 0;
    int[] colors = {0, 0, 0};
    while (!stop) {
      colors[0] = colors[1] = colors[2] = 0;
      colors[index] = 255;
      for (int i = 0; i < 8; i++) {
        runner.setLed(i, colors[0], colors[1], colors[2]);
        try {
          TimeUnit.MILLISECONDS.sleep(100);
        } catch (InterruptedException ex) {

        }
        runner.setLed(i, 0, 0, 0);
      }
      try {
        TimeUnit.MILLISECONDS.sleep(500);
      } catch (InterruptedException ex) {

      }
      index = (index + 1) % 3;
    }
    try {
      TimeUnit.MILLISECONDS.sleep(2000);
    } catch (InterruptedException ex) {

    }
  }
}