package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms;

import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.exceptions.TooBigMessage;
import java.awt.Color;
import java.io.IOException;

/**
 * Interface for class that is running tasks offering several useful methods to algorithm.
 */
public interface TaskRunner {
  /**
   * Called by algorithm to send status update.
   *
   * @param status String describing status.
   */
  void sendStatusUpdate(String status);

  /**
   * Used by task to set specific LED to specific color.
   *
   * @param index Index of the LED.
   * @param color Desired color of the LED.
   */
  void setLed(int index, Color color);

  /**
   * Used by task to set specific LED to specific color using RGB. The individual colors must have values from 0 to 255.
   *
   * @param index Index of the LED.
   * @param red   Red part of the color.
   * @param green Green part of the color.
   * @param blue  Blue part of the color.
   */
  void setLed(int index, int red, int green, int blue);

  /**
   * Called by task when it stops.
   */
  void taskStopped();

  /**
   * Called by task to get a list of UUID that are his coworkers on the task.
   *
   * @return List of coworkers UUIDs.
   */
  String[] getCoworkers();

  /**
   * Used by task to send message via UDP to another node. The delivery nor correctness is guaranteed.
   *
   * @param uuid    UUID of the receiving node.
   * @param content Content of the message.
   * @throws TooBigMessage When the message is too big.
   */
  void sendUdpMessage(String uuid, String content) throws TooBigMessage;

  /**
   * Used by task to send message to another node using TCP.
   *
   * @param uuid    UUID of the receiving node.
   * @param content Content of the message.
   * @throws IOException When sending goes wrong.
   */
  void sendTcpMessage(String uuid, String content) throws IOException;

  /**
   * Used by node to send message to the server part of the algorithm.
   *
   * @param content Content of the message.
   * @throws IOException when sending goes wrong.
   */
  void sendMessageToServer(String content) throws IOException;

  /**
   * Used to download distributed data from server.
   *
   * @param identifier Identifier of the data.
   * @return Downloaded data.
   */
  String fetchData(String identifier);
}