package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.tcpFailTester;

import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.AbstractAlgorithm;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.ServerPartListener;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.TaskRunner;
import java.io.File;
import java.util.HashMap;
import java.util.logging.Logger;

/**
 * Algorithm for testing the UDP communication between nodes.
 *
 * @author Roman Janků (jankurom@fel.cvut.cz)
 */
public class TcpFailTester extends AbstractAlgorithm {

  private static final Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
  private volatile boolean waiting;

  @Override
  public void receivedUdpMessage(String uuid, String content) {

  }

  @Override
  public void nodeUnreachable(String uuid) {
    waiting = false;
  }

  @Override
  public void receivedTcpMessage(String uuid, String content) {

  }

  @Override
  public HashMap<String, File> getDistributedFiles() {
    return new HashMap<>();
  }

  @Override
  public void stop() {

  }

  @Override
  public String run(String[] parameters, TaskRunner taskRunner) throws Exception {
    waiting = true;
    taskRunner.sendTcpMessage("nonsense", "does not matter");
    while (waiting) {
    }
    return "Successful";
  }

  @Override
  public String composeResult(String[] partialResults) {
    return "TCP error is working!";
  }

  @Override
  public String name() {
    return "TCP error tester";
  }

  @Override
  public void serverPart(ServerPartListener listener, String[] uuids, String[] parameters) {

  }

  @Override
  public void nodeReceivedMessageFromServer(String content) {

  }

  @Override
  public void serverReceivedMessageFromNode(String content, String uuid) {

  }

  @Override
  public String description() {
    return "Tests tcp error handling.";
  }

  @Override
  public String[] parameters() {
    return new String[0];
  }
}