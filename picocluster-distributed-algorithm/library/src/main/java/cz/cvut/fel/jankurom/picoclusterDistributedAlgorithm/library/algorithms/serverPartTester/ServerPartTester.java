package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.serverPartTester;

import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.AbstractAlgorithm;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.ServerPartListener;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.TaskRunner;
import java.io.File;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

/**
 * Algorithm for testing the UDP communication between nodes.
 *
 * @author Roman Janků (jankurom@fel.cvut.cz)
 */
public class ServerPartTester extends AbstractAlgorithm {

  private static final Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
  private volatile boolean waiting;
  private volatile int recieved;

  @Override
  public void receivedUdpMessage(String uuid, String content) {

  }

  @Override
  public void serverPart(ServerPartListener listener, String[] uuids, String[] parameters) {
    while (recieved != uuids.length) {
    }

    for (String uuid : uuids) {
      listener.sendMessageToNode(uuid, "I have everything!");
    }
  }

  @Override
  public void nodeReceivedMessageFromServer(String content) {
    waiting = false;
  }

  @Override
  public void serverReceivedMessageFromNode(String content, String uuid) {
    recieved++;
  }

  @Override
  public void nodeUnreachable(String uuid) {

  }

  @Override
  public void receivedTcpMessage(String uuid, String content) {

  }

  @Override
  public HashMap<String, File> getDistributedFiles() {
    return new HashMap<>();
  }

  @Override
  public void stop() {

  }

  @Override
  public String run(String[] parameters, TaskRunner taskRunner) throws Exception {
    waiting = true;
    TimeUnit.MILLISECONDS.sleep(5000);
    taskRunner.sendMessageToServer("Node here!");
    while (waiting) {
    }
    return "done!";
  }

  @Override
  public String composeResult(String[] partialResults) {
    return "Server part is working!";
  }

  @Override
  public String name() {
    return "Server part tester";
  }

  @Override
  public String description() {
    return "Tests functionality of server part.";
  }

  @Override
  public String[] parameters() {
    return new String[0];
  }
}