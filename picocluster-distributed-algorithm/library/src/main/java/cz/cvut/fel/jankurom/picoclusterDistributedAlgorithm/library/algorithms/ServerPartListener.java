package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms;

import java.net.InetAddress;

public interface ServerPartListener {
  /**
   * Sends message to the node with given UUID.
   *
   * @param uuid    UUID of the receiving node.
   * @param content Content of the message.
   */
  void sendMessageToNode(String uuid, String content);

  InetAddress getAddressForUuid(String uuid);
}
