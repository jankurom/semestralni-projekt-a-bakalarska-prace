package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.exceptions;

/**
 * Thrown when HandlerAccessor cannot find handler for given MessageType.
 *
 * @author Roman Janků (jankurom@fel.cvut.cz)
 */
public class NoHandlerForMessageTypeException extends Exception {
  public NoHandlerForMessageTypeException() {
  }

  public NoHandlerForMessageTypeException(String message) {
    super(message);
  }
}
