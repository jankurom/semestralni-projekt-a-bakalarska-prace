package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.sleep;

import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.AbstractAlgorithm;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.ServerPartListener;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.TaskRunner;
import java.io.File;
import java.util.HashMap;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

/**
 * Algorithm sleeping for random amount of time.
 *
 * @author Roman Janků (jankurom@fel.cvut.cz)
 */
public class Sleep extends AbstractAlgorithm {
  @Override
  public void stop() {

  }

  @Override
  public void receivedUdpMessage(String uuid, String content) {

  }

  @Override
  public void serverPart(ServerPartListener listener, String[] uuids, String[] parameters) {

  }

  @Override
  public void nodeReceivedMessageFromServer(String content) {

  }

  @Override
  public void serverReceivedMessageFromNode(String content, String uuid) {

  }

  @Override
  public void nodeUnreachable(String uuid) {

  }

  @Override
  public void receivedTcpMessage(String uuid, String content) {

  }

  @Override
  public String run(String[] parameters, TaskRunner taskRunner) throws InterruptedException {
    long sleepTime = ThreadLocalRandom.current().nextLong(Long.parseLong(parameters[0]), Long.parseLong(parameters[1]) + 1);
    TimeUnit.MILLISECONDS.sleep(sleepTime);
    return "I have slept for " + sleepTime + " milliseconds.";
  }

  @Override
  public String composeResult(String[] partialResults) {
    return "All " + partialResults.length + " have finished sleeping.";
  }

  @Override
  public String name() {
    return "Sleeping task";
  }

  @Override
  public String description() {
    return "Node sleeps for random time and then finishes.";
  }

  @Override
  public HashMap<String, File> getDistributedFiles() {
    return new HashMap<>();
  }

  @Override
  public String[] parameters() {
    return new String[]{"Minimum sleep time (ms)", "Maximum sleep time (ms)"};
  }
}