package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.responseMessage;

import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.AbstractAlgorithm;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.ServerPartListener;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.TaskRunner;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.exceptions.TooBigMessage;
import java.io.File;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Algorithm testing the response time on UDP messages.
 *
 * @author Roman Janků (jankurom@fel.cvut.cz)
 */
public class ResponseMessage extends AbstractAlgorithm {

  private volatile LocalTime[] sent;
  private volatile List<Long> delay;
  private TaskRunner runner;

  @Override
  public void receivedUdpMessage(String uuid, String content) {
    if (content == null) {
      return;
    }
    if (content.startsWith("Q")) {
      try {
        runner.sendUdpMessage(uuid, "A" + content.substring(1));
      } catch (TooBigMessage ex) {
        // ignored, nothing we can do
      }
    } else {
      var parts = content.split("@");
      if (parts[1] != null) {
        delay.add(sent[Integer.parseInt(parts[1])].until(LocalTime.now(), ChronoUnit.MILLIS));
      }
    }
  }

  @Override
  public void serverPart(ServerPartListener listener, String[] uuids, String[] parameters) {

  }

  @Override
  public void nodeReceivedMessageFromServer(String content) {

  }

  @Override
  public void serverReceivedMessageFromNode(String content, String uuid) {

  }

  @Override
  public void nodeUnreachable(String uuid) {

  }

  @Override
  public void receivedTcpMessage(String uuid, String content) {

  }

  @Override
  public void stop() {

  }

  @Override
  public String run(String[] parameters, TaskRunner taskRunner) throws Exception {
    runner = taskRunner;

    var size = Integer.parseInt(parameters[0]);
    var interval = Integer.parseInt(parameters[1]);
    var messages = Integer.parseInt(parameters[2]);

    var total = messages * taskRunner.getCoworkers().length;

    sent = new LocalTime[total];
    delay = new ArrayList<>();

    var sequential = 0;

    var sb = new StringBuilder();
    for (int i = 0; i < size; i++) {
      sb.append('A');
    }
    var ballast = sb.toString();

    TimeUnit.MILLISECONDS.sleep(2000);

    for (int i = 0; i < messages; i++) {
      for (var coworker : taskRunner.getCoworkers()) {
        sent[sequential] = LocalTime.now();
        taskRunner.sendUdpMessage(coworker, new StringBuilder().append("Q@").append(sequential).append('@').append(ballast).toString());
        sequential++;
      }
      TimeUnit.MILLISECONDS.sleep(interval);
    }

    TimeUnit.MILLISECONDS.sleep(10000);

    var average = delay.stream().reduce((long) 0, (x, y) -> x + y) / delay.size();
    return average + "@" + delay.size() + "@" + total;
  }

  @Override
  public String composeResult(String[] partialResults) {
    var received = 0;
    var sent = 0;
    var sum = 0;

    for (var res : partialResults) {
      var parts = res.split("@");
      sum += Long.parseLong(parts[0]) * Long.parseLong(parts[1]);
      received += Long.parseLong(parts[1]);
      sent += Long.parseLong(parts[2]);
    }

    var average = sum / received;

    return "Received " + received + " out of " + sent + " with delay " + average + " ms.";
  }

  @Override
  public String name() {
    return "Response measurement task";
  }

  @Override
  public String description() {
    return "Task for measuring the response time network.";
  }

  @Override
  public HashMap<String, File> getDistributedFiles() {
    return new HashMap<>();
  }

  @Override
  public String[] parameters() {
    return new String[]{"Added size to the message (Bytes)", "Interval between messages (ms)", "Number of messages"};
  }
}