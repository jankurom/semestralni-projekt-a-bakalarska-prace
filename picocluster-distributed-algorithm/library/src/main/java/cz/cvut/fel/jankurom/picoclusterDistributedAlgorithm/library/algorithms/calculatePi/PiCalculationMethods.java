package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.calculatePi;

import java.util.Random;

/**
 * Class with methods for calculating PI.
 */
final class PiCalculationMethods {

  private static final Random RANDOM = new Random();

  private PiCalculationMethods() {

  }

  /**
   * Returns PI from Java standard library.
   *
   * @return PI from Java standard library.
   */
  public static double javaMathPi() {
    return Math.PI;
  }

  /**
   * Approximates PI using Gregory-Leibnitz series.
   *
   * @return Approximation of PI.
   */
  public static double gregoryLeibnitzSeries() {
    double sum = 0;
    for (int i = 0; i < 1000; i++) {
      sum += (Math.pow(-1, i)) / (double) (2 * i + 1);
    }
    return 4 * sum;
  }

  /**
   * Approximates PI using Nilakantha series.
   *
   * @return Approximation of PI.
   */
  public static double nilakanthaSeries() {
    double sum = 0;
    for (int i = 0; i < 25; i++) {
      sum += (Math.pow(-1, i)) / ((2 * i + 2) * (2 * i + 3) * (2 * i + 4));
    }
    return 3 + 4 * sum;
  }

  /**
   * Approximates PI by calculating limit at maximum integer.
   *
   * @return Approximation of PI.
   */
  public static double limit() {
    return (double) Integer.MAX_VALUE * Math.sin(Math.toRadians((double) 180 / (double) Integer.MAX_VALUE));
  }

  /**
   * Calculates PI using inverse sine.
   *
   * @return Value of PI.
   */
  public static double inverseSine() {
    double x = RANDOM.nextDouble() * 2 - 1;
    return 2 * (Math.asin(Math.sqrt(1 - Math.pow(x, 2))) + Math.abs(Math.asin(x)));
  }
}