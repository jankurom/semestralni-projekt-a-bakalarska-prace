package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.textDisplay;

import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.AbstractAlgorithm;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.ServerPartListener;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.TaskRunner;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * Algorithm sleeping for random amount of time.
 *
 * @author Roman Janků (jankurom@fel.cvut.cz)
 */
public class TextDisplay extends AbstractAlgorithm {

  private volatile boolean run = true;
  private int[] color = {0, 254, 0};
  private int line = 0;

  private long timeout = 2500;

  private TaskRunner runner;

  private static final HashMap<String, int[]> bits = new HashMap<>();

  static {
    bits.put("A", new int[]{24, 36, 66, 126, 66});
    bits.put("a", new int[]{24, 4, 28, 36, 28});
    bits.put("B", new int[]{56, 36, 56, 36, 56});
    bits.put("b", new int[]{32, 32, 56, 36, 56});
    bits.put("C", new int[]{60, 32, 32, 32, 60});
    bits.put("c", new int[]{0, 28, 32, 32, 28});
    bits.put("D", new int[]{56, 36, 36, 36, 56});
    bits.put("d", new int[]{4, 4, 28, 36, 28});
    bits.put("E", new int[]{60, 32, 56, 32, 60});
    bits.put("e", new int[]{24, 36, 60, 32, 28});
    bits.put("F", new int[]{60, 32, 56, 32, 32});
    bits.put("f", new int[]{24, 32, 48, 32, 32});
    bits.put("G", new int[]{60, 32, 44, 36, 60});
    bits.put("g", new int[]{24, 36, 28, 4, 24});
    bits.put("H", new int[]{36, 36, 60, 36, 36});
    bits.put("h", new int[]{32, 32, 56, 36, 36});
    bits.put("I", new int[]{8, 8, 8, 8, 8});
    bits.put("i", new int[]{16, 0, 16, 16, 16});
    bits.put("J", new int[]{8, 8, 8, 40, 24});
    bits.put("j", new int[]{16, 0, 16, 16, 48});
    bits.put("K", new int[]{36, 40, 48, 40, 36});
    bits.put("k", new int[]{32, 32, 40, 48, 40});
    bits.put("L", new int[]{32, 32, 32, 32, 60});
    bits.put("l", new int[]{32, 32, 32, 32, 16});
    bits.put("M", new int[]{54, 42, 34, 34, 34});
    bits.put("m", new int[]{0, 128, 236, 146, 146});
    bits.put("N", new int[]{34, 50, 42, 38, 34});
    bits.put("n", new int[]{0, 0, 56, 36, 36});
    bits.put("O", new int[]{24, 36, 36, 36, 24});
    bits.put("o", new int[]{0, 24, 36, 36, 24});
    bits.put("P", new int[]{56, 36, 56, 32, 32});
    bits.put("p", new int[]{0, 56, 36, 56, 32});
    bits.put("Q", new int[]{24, 36, 36, 44, 28});
    bits.put("q", new int[]{0, 28, 36, 28, 4});
    bits.put("R", new int[]{56, 36, 56, 40, 36});
    bits.put("r", new int[]{0, 44, 48, 32, 32});
    bits.put("S", new int[]{28, 32, 24, 4, 56});
    bits.put("s", new int[]{28, 32, 24, 4, 56});
    bits.put("T", new int[]{62, 8, 8, 8, 8});
    bits.put("t", new int[]{16, 56, 16, 16, 8});
    bits.put("U", new int[]{36, 36, 36, 36, 24});
    bits.put("u", new int[]{0, 36, 36, 36, 28});
    bits.put("V", new int[]{34, 34, 20, 20, 8});
    bits.put("v", new int[]{34, 34, 20, 8});
    bits.put("W", new int[]{73, 73, 42, 42, 20});
    bits.put("w", new int[]{0, 0, 42, 42, 20});
    bits.put("X", new int[]{68, 40, 16, 40, 68});
    bits.put("x", new int[]{0, 0, 20, 8, 20});
    bits.put("Y", new int[]{68, 40, 16, 16, 16});
    bits.put("y", new int[]{0, 20, 12, 4, 24});
    bits.put("Z", new int[]{60, 4, 24, 32, 60});
    bits.put("z", new int[]{0, 24, 8, 16, 24});
    bits.put("0", new int[]{24, 44, 52, 36, 24});
    bits.put("1", new int[]{24, 8, 8, 8, 28});
    bits.put("2", new int[]{24, 4, 24, 32, 60});
    bits.put("3", new int[]{24, 4, 24, 4, 24});
    bits.put("4", new int[]{8, 24, 40, 60, 8});
    bits.put("5", new int[]{60, 32, 56, 4, 56});
    bits.put("6", new int[]{24, 32, 56, 36, 24});
    bits.put("7", new int[]{60, 4, 8, 8, 8});
    bits.put("8", new int[]{24, 36, 24, 36, 24});
    bits.put("9", new int[]{24, 36, 28, 4, 24});
    bits.put(".", new int[]{0, 0, 0, 8, 0});
    bits.put(",", new int[]{0, 0, 0, 8, 16});
    bits.put(":", new int[]{0, 8, 0, 8, 0});
    bits.put(";", new int[]{0, 8, 0, 8, 16});
    bits.put("|", new int[]{8, 8, 8, 8, 8});
    bits.put("-", new int[]{0, 0, 60, 0, 0});
    bits.put("/", new int[]{8, 8, 16, 32, 32});
    bits.put("\\", new int[]{32, 32, 16, 8, 8});
  }

  @Override
  public void stop() {
    run = false;
  }

  @Override
  public void receivedUdpMessage(String uuid, String content) {

  }

  @Override
  public void serverPart(ServerPartListener listener, String[] uuids, String[] parameters) {
    var list = new ArrayList<Pair<String, InetAddress>>();
    Arrays.stream(uuids).forEach(id -> list.add(new Pair<>(id, listener.getAddressForUuid(id))));
    var sorted = list.stream().sorted((o1, o2) -> {
      String[] ip1 = o1.second.toString().split("\\.");
      String ipFormatted1 = String.format("%3s.%3s.%3s.%3s", ip1[0],ip1[1],ip1[2],ip1[3]);
      String[] ip2 = o2.second.toString().split("\\.");
      String ipFormatted2 = String.format("%3s.%3s.%3s.%3s",  ip2[0],ip2[1],ip2[2],ip2[3]);
      return ipFormatted1.compareTo(ipFormatted2);
    }).collect(Collectors.toList());

    for (int i = 0; i < 5; i++) {
      listener.sendMessageToNode(sorted.get(i).first, "LINE:" + i);
    }

    try {
      TimeUnit.MILLISECONDS.sleep(1000);
    } catch (InterruptedException e) {
      throw new RuntimeException(e);
    }

    boolean readingColor = false;
    boolean readingTime = false;
    StringBuilder color = new StringBuilder();
    StringBuilder time = new StringBuilder();

    try {
      for (char ch: parameters[0].toCharArray()) {
        if (readingColor) {
          if (ch == ']') {
            readingColor = false;
            String finalColor = color.toString();
            sorted.forEach(node -> listener.sendMessageToNode(node.first, "COLOR:" + finalColor));
            color = new StringBuilder();
          } else {
            color.append(ch);
          }
        } else if (readingTime) {
          if (ch == '}') {
            readingTime = false;
            timeout = Integer.parseInt(time.toString());
            time = new StringBuilder();
          } else {
            time.append(ch);
          }
        } else {
          if (ch == '[') {
            readingColor = true;
          } else if (ch == '{') {
            readingTime = true;
          } else {
            sorted.forEach(node -> listener.sendMessageToNode(node.first, "CHAR:" + ch));
            TimeUnit.MILLISECONDS.sleep(timeout);
          }
        }
      }
    } catch (InterruptedException e) {
      System.out.println(e.getMessage());
    }

    sorted.forEach(node -> listener.sendMessageToNode(node.first, "END:END"));
  }

  @Override
  public void nodeReceivedMessageFromServer(String content) {
    var string = content.split(":");
    var command = string[0].trim();
    var data = string[1].trim();
    runner.sendStatusUpdate("Received: " + content);

    switch (command) {
      case "CHAR":
        displayChar(data);
        break;
      case "LINE":
        line = Integer.parseInt(data);
        break;
      case "END":
        run = false;
        break;
      case "COLOR":
        var items = data.split(",");
        color[0] = Integer.parseInt(items[0]);
        color[1] = Integer.parseInt(items[1]);
        color[2] = Integer.parseInt(items[2]);
        break;
    }
  }

  private void displayChar(String toDisplay) {
    var displayData = bits.get(toDisplay);
    if (displayData != null) {
      var displayLine = displayData[line];
      for (int i = 0; i < 8; i++) {
        if ((displayLine & (1 << i)) != 0) {
          runner.setLed(i, color[0], color[1], color[2]);
        } else {
          runner.setLed(i, 0, 0, 0);
        }
      }
    } else {
      runner.sendStatusUpdate("No data for " + toDisplay);
      for (int i = 0; i < 8; i++) {
        runner.setLed(i, 0, 0, 0);
      }
    }
  }

  @Override
  public void serverReceivedMessageFromNode(String content, String uuid) {

  }

  @Override
  public void nodeUnreachable(String uuid) {

  }

  @Override
  public void receivedTcpMessage(String uuid, String content) {

  }

  @Override
  public String run(String[] parameters, TaskRunner taskRunner) throws InterruptedException {
    this.runner = taskRunner;
    while(run) {
    }
    return "Text was displayed.";
  }

  @Override
  public String composeResult(String[] partialResults) {
    return "All chars were displayed.";
  }

  @Override
  public String name() {
    return "Displaying text";
  }

  @Override
  public String description() {
    return "RaspberryPis displaying text from file. It correctly works only on 5 rPis with IPv4 addressing!";
  }

  @Override
  public HashMap<String, File> getDistributedFiles() {
    return new HashMap<>();
  }

  @Override
  public String[] parameters() {
    return new String[]{"Text to display"};
  }

  private class Pair<K, V> {
    K first;
    V second;
    public Pair(K first, V second) {
      this.first = first;
      this.second = second;
    }

    public K getFirst() {
      return first;
    }

    public void setFirst(K first) {
      this.first = first;
    }

    public V getSecond() {
      return second;
    }

    public void setSecond(V second) {
      this.second = second;
    }
  }
}