package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Objects;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import org.reflections.Reflections;

/**
 * Class for handling distributed algorithms
 *
 * @author Roman Janků (jankurom@fel.cvut.cz)
 */
public final class Algorithms {

  private static final Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
  /**
   * Reflection for easier access to classes.
   */
  private static final Reflections REFLECTIONS =
      new Reflections("cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms");

  private Algorithms() {

  }

  /**
   * Loads all available algorithms.
   *
   * @return List of all available algorithms.
   */
  public static List<AbstractAlgorithm> getAllAlgorithms() {
    return REFLECTIONS.getSubTypesOf(AbstractAlgorithm.class).stream()
        .map(c -> {
          try {
            return c.getDeclaredConstructor().newInstance();
          } catch (InstantiationException | IllegalAccessException | InvocationTargetException |
                   NoSuchMethodException ex) {
            LOGGER.warning(() -> "Could not load '" + c.getName() + "'. Cause: " + ex.getMessage());
          }
          return null;
        })
        .filter(Objects::nonNull)
        .collect(Collectors.toList());
  }
}