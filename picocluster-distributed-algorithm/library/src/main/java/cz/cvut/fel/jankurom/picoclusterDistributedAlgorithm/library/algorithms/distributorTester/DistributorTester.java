package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.distributorTester;

import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.AbstractAlgorithm;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.ServerPartListener;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.TaskRunner;
import java.io.File;
import java.util.HashMap;

/**
 * Algorithm that tests the distribution function on the server.
 *
 * @author Roman Janků (jankurom@fel.cvut.cz)
 */
public class DistributorTester extends AbstractAlgorithm {
  @Override
  public void stop() {

  }

  @Override
  public void receivedUdpMessage(String uuid, String content) {

  }

  @Override
  public void serverPart(ServerPartListener listener, String[] uuids, String[] parameters) {

  }

  @Override
  public void nodeReceivedMessageFromServer(String content) {

  }

  @Override
  public void serverReceivedMessageFromNode(String content, String uuid) {

  }

  @Override
  public void nodeUnreachable(String uuid) {

  }

  @Override
  public void receivedTcpMessage(String uuid, String content) {

  }

  @Override
  public String run(String[] parameters, TaskRunner taskRunner) throws Exception {
    var ret = taskRunner.fetchData("distributorTester.data");
    if (ret == null) {
      throw new Exception("Failed!");
    }
    return ret;
  }

  @Override
  public String composeResult(String[] partialResults) {
    for (String res : partialResults) {
      if (!res.equals("testing")) {
        return "failed";
      }
    }
    return "successful";
  }

  @Override
  public String name() {
    return "Distributor tester task";
  }

  @Override
  public String description() {
    return "Downloads data from server using Distributor and returns the to the server via result.";
  }

  @Override
  public HashMap<String, File> getDistributedFiles() {
    var map = new HashMap<String, File>();
    map.put("distributorTester.data", new File("distributorTester"));
    return map;
  }

  @Override
  public String[] parameters() {
    return new String[0];
  }
}