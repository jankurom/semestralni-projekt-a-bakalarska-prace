package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms;

import java.io.File;
import java.util.Map;

/**
 * Defines unified interface for algorithms that are run using reflection.
 *
 * @author Roman Janků
 */
public abstract class AbstractAlgorithm {
  /**
   * Runs the algorithm with given parameters.
   *
   * @param parameters Parameters for the algorithm.
   */
  public abstract String run(String[] parameters, TaskRunner taskRunner) throws Exception;

  /**
   * Composes partial results returned by nodes to a one result of the distributed algorithm.
   *
   * @param partialResults Array of partial results returned by the nodes.
   * @return One composed result made out of partial results of the distributed algorithm.
   */
  public abstract String composeResult(String[] partialResults);

  /**
   * Returns name of the algorithm.
   *
   * @return Name of the algorithm.
   */
  public abstract String name();

  /**
   * Returns description of the algorithm.
   *
   * @return Description of the algorithm.
   */
  public abstract String description();

  /**
   * Returns description of parameters for the algorithm. Should return something like "[0] Integer: number of iterations; [2] String: ...".
   *
   * @return Description of parameters for the algorithm.
   */
  public abstract String[] parameters();

  /**
   * Called to stop the algorithm.
   */
  public abstract void stop();

  /**
   * Called when node receives UDP message from another node.
   *
   * @param uuid    UUID of the node sending the message.
   * @param content Content of the message.
   */
  public abstract void receivedUdpMessage(String uuid, String content);

  /**
   * Called when node receives TCP message from another node.
   *
   * @param uuid    UUID of the sender.
   * @param content Content of the message.
   */
  public abstract void receivedTcpMessage(String uuid, String content);

  /**
   * Called when TCP message cannot be delivered due to unknown recipient.
   *
   * @param uuid unknown UUID.
   */
  public abstract void nodeUnreachable(String uuid);

  /**
   * Returns list of distributed files that will be loaded by server and will be available to clients to download.
   *
   * @return Hash map of string identification and file pointer pairs.
   */
  public abstract Map<String, File> getDistributedFiles();

  /**
   * Method containing server part of the algorithm that is run only once on server.
   *
   * @param listener   Listener that provides additional functionality for the server part of the algorithm.
   * @param uuids      Array with UUIDs of all nodes that are participating in this distributed algorithm.
   * @param parameters Parameters for the algorithm that user has entered.
   */
  public abstract void serverPart(ServerPartListener listener, String[] uuids, String[] parameters);


  /**
   * Called when node receives message server part of the algorithm.
   *
   * @param content Content of the message
   */
  public abstract void nodeReceivedMessageFromServer(String content);

  /**
   * Called when server part of the algorithm receives message from node.
   *
   * @param content Content of the message.
   * @param uuid    UUID of the node that sent the message.
   */
  public abstract void serverReceivedMessageFromNode(String content, String uuid);

  @Override
  public String toString() {
    return name();
  }
}