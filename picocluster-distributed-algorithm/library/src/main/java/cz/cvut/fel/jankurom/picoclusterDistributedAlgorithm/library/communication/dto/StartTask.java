package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.dto;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * DTO for start message that contains task and parameters that node should run. This class is immutable.
 *
 * @author Roman Janků (jankurom@fel.cvut.cz)
 */
public class StartTask {
  private final String taskName;
  private final List<String> parameters;
  private final List<String> network;

  /**
   * Create a new StartTask
   *
   * @param taskName   Full name of the algorithm class with packages.
   * @param parameters Parameters for the algorithm.
   */
  public StartTask(String taskName, String[] parameters, String[] network) {
    this.taskName = taskName;
    this.parameters = new ArrayList<>();
    this.parameters.addAll(Arrays.asList(parameters));
    this.network = new ArrayList<>();
    this.network.addAll(Arrays.asList(network));
  }

  /**
   * Returns the full name of the algorithm class with packages.
   *
   * @return Full class name of the algorithm.
   */
  public String getTaskName() {
    return taskName;
  }

  /**
   * Returns array of parameters for the algorithm.
   *
   * @return Array of parameters.
   */
  public List<String> getParameters() {
    return parameters;
  }

  public List<String> getNetwork() {
    return network;
  }
}