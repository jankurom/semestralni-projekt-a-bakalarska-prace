package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.dto;

/**
 * DTO of the request for distributed data.
 */
public class DistributorRequest {
  private String key;

  public DistributorRequest(String key) {
    this.key = key;
  }

  public String getKey() {
    return key;
  }

  public void setKey(String key) {
    this.key = key;
  }
}