package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication;

/**
 * Message class that defines JSON structure of messages being sent between clients and server.
 *
 * @author Roman Janků (jankurom@fel.cvut.cz)
 */
public class Message {
  private final String from;
  private final String to;
  private final String content;
  private final MessageType type;

  /**
   * Creates a new message from given parameters.
   *
   * @param from    UUID of the sender of the message.
   * @param to      UUID of the recipient of the message.
   * @param content Content of the message.
   * @param type    Type of the message.
   */
  public Message(String from, String to, String content, MessageType type) {
    this.from = from;
    this.to = to;
    this.content = content;
    this.type = type;
  }

  /**
   * Returns the UUID of the sender.
   *
   * @return UUID of the sender.
   */
  public String getFrom() {
    return from;
  }

  /**
   * Returns the UUID of the recipient.
   *
   * @return UUID of the recipient.
   */
  public String getTo() {
    return to;
  }

  /**
   * Returns the content of the message.
   *
   * @return Content of the message.
   */
  public String getContent() {
    return content;
  }

  /**
   * Returns the type of the message.
   *
   * @return Type of the message.
   */
  public MessageType getType() {
    return type;
  }
}
