package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication;

/**
 * Message type definition.
 *
 * @author Roman Janků (jankurom@fel.cvut.cz)
 */
public enum MessageType {
  /**
   * Node requests server to assign UUID.
   */
  UUID_REQUEST,
  /**
   * Server confirms node's UUID.
   */
  UUID_CONFIRM,
  /**
   * Server tells nodes to start task.
   */
  START,
  /**
   * Node tells server that he finished task.
   */
  FINISHED,
  /**
   * Something bad happened and we want to notify the other side.
   */
  ERROR,
  /**
   * Server telling nodes to stop computation.
   */
  STOP,
  /**
   * Node failed the task.
   */
  FAILED,
  /**
   * Server sends client information about all ready clients.
   */
  NODES,
  /**
   * Nodes informs server about current status of the computation.
   */
  STATUS,
  /**
   * Send by client to the server when the client stopped the algorithm.
   */
  STOPPED,
  /**
   * Used by client to inform server he had sent a message.
   */
  SENT,
  /**
   * Used by client to send server UDP port for communication.
   */
  UDP_PORT,
  /**
   * Used by clients to communicate between them using server.
   */
  DATA,
  /**
   * Sent by server when recipient of the message is unreachable.
   */
  UNREACHABLE,
  /**
   * Sent by server to send data from server part of the algorithm to the node.
   */
  SERVER_TO_NODE,
  /**
   * Sent by the node to send data from node to the server part of the algorithm.
   */
  NODE_TO_SERVER
}