package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.exceptions;

/**
 * Throw whe the UDP message is too big.
 */
public class TooBigMessage extends Exception {
}