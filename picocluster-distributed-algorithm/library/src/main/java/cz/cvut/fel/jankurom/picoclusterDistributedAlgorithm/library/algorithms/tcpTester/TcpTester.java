package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.tcpTester;

import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.AbstractAlgorithm;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.ServerPartListener;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.TaskRunner;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

/**
 * Algorithm for testing the UDP communication between nodes.
 *
 * @author Roman Janků (jankurom@fel.cvut.cz)
 */
public class TcpTester extends AbstractAlgorithm {

  private static final Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
  private Map<String, Boolean> received;
  private volatile boolean waiting;
  private volatile boolean failed;

  @Override
  public void receivedUdpMessage(String uuid, String content) {

  }

  @Override
  public void serverPart(ServerPartListener listener, String[] uuids, String[] parameters) {

  }

  @Override
  public void nodeReceivedMessageFromServer(String content) {

  }

  @Override
  public void serverReceivedMessageFromNode(String content, String uuid) {

  }

  @Override
  public void nodeUnreachable(String uuid) {

  }

  @Override
  public void receivedTcpMessage(String uuid, String content) {
    synchronized (received) {
      if (received.containsKey(uuid)) {
        received.put(uuid, true);
      }
    }

    LOGGER.info(() -> "Waiting for " + received.values().stream().filter(v -> !v).count() + "/" + received.size());

    if (received.values().stream().noneMatch(v -> !v)) {
      waiting = false;
    }
  }

  @Override
  public HashMap<String, File> getDistributedFiles() {
    return new HashMap<>();
  }

  @Override
  public void stop() {

  }

  @Override
  public String run(String[] parameters, TaskRunner taskRunner) throws Exception {
    waiting = true;
    failed = false;
    received = new HashMap<>();
    Arrays.stream(taskRunner.getCoworkers()).forEach(c -> {
      received.put(c, false);
      LOGGER.info(() -> "Putting " + c + " in the received with false.");
    });

    TimeUnit.MILLISECONDS.sleep(5000);
    Arrays.stream(taskRunner.getCoworkers()).forEach(c -> {
      try {
        taskRunner.sendTcpMessage(c, "test");
      } catch (IOException e) {
        failed = true;
      }
    });
    if (failed) {
      throw new Exception("Could not send message!");
    }
    while (waiting) {
    }
    return "I have all messages";
  }

  @Override
  public String composeResult(String[] partialResults) {
    return "All communications are working!";
  }

  @Override
  public String name() {
    return "TCP tester";
  }

  @Override
  public String description() {
    return "Sends all possible TCP messages between nodes.";
  }

  @Override
  public String[] parameters() {
    return new String[0];
  }
}